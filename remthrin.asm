TITLE remthrin          ; 64K threaded interpreter REAL MODE version
    .MODEL tiny         ; 16-bit single segment model ".COM"
    .DOSSEG             ; Force DOS segment order
    .STACK

    .DATA               ; Data segment
    WORD 8 DUP (?)      ; buffer parameter stack underflow space
    WORD 0BAD1h         ; underflow stack item alarm
pstackspace WORD 1024 DUP (?) ; internal parameter stack 1K location
stackptrset WORD ?      ; stack pointer start
assembletohere WORD ?   ; start of system built items

    .CODE
    .STARTUP            ; Initialize data seg. and set SS = DS
    ORG 100h
    mov stackptrset, sp
    mov ax, offset assemblefromhere
    mov assembletohere, ax
    jmp initialrestartc

rootVleadin BYTE 0, 0   ; "crippled" lead vocabulary entry referenced 
    dw offset en00
rootVdirset WORD noexe  ; referenced in dir for lead rootv vocabulary

    ; ----- r e s t a r t
    frameset word ?
    filereadinterrupt byte ?
    en00 BYTE 7, "restart", 0 ;
    dw offset en00r
    WORD initialrestartc ; restart code, interpreter for data following
initialrestartc:
    mov sp, stackptrset
    mov bp, offset pstackspace
    
    mov bx, offset exitOrRestartc
    push bx ; save recovery option before exit program from "falling out"
    mov bx, offset filereadinterrupt
    mov byte ptr [bx], 1 ; interrupt any noninteractive mode


    mov bx, offset loopEI
    mov ax, bx
    jmp WORD ptr [bx] ;


    ; ----- __ c h I n t e r p __
    xc00 BYTE 8, "chInterp", 0
    dw offset xc00a
    WORD changexpc      ; itself considered an offset self interpreter
changexp WORD THIS WORD + 2 ; hense, this indirect indication see "changexc"
			; expect param stack with ptr to interp
changexpc: sub bp, 2    ; \ to change
    mov bx, [bp]        ; pop param stack via pre-decrement

    add ax, 2           ; employ with ax pointing to callee location
	mov si, ax      ; ax pointing to changexp
	mov ax, [si]    ; ax now pointing to "sub bp, 2" this routine
	
	mov [bx], ax    ; change interpreter
	pop bx
	jmp bx


    ; ----- __ m a c h i n e c o d e __

    xc00a BYTE 11, "machinecode", 0
    dw offset xc01
    WORD changexc          ; on entry, ax points to calling location
machinecode: add ax, 2                  ; advance to first word in param list
    mov bx, ax
    jmp bx






loopEI  WORD colon
	WORD execinterp
	WORD exitOrRestart
	WORD back2
back2   WORD this word + 2              ; this simple routine to make this
	pop cx                          ; an infinite loop
	pop bx
	sub bx, 6
	push bx
	jmp colonext

; TODO whitespace align TAB handler

recoverystkpoint WORD ?                 ; recovery stack position for restarts
quitmsg BYTE "exit program ?$"          ; "underneath" stack will lie this
ynansmax BYTE 2                         ; \ routine to exit program.
ynansz  BYTE 2
ynansr  BYTE ?
	BYTE ?                          ; space for carriage return in answer


exitOrRestart WORD OFFSET exitOrRestartc
exitOrRestartc: mov dx, OFFSET quitmsg
	xor al, al                      ; if "Q" entered to leave shells,
	xor bx, bx                      ; \ verify from user to leave
	mov dx, OFFSET quitmsg          ; \ final shell and abort program
	mov ah, 9                       ;      dos routine strout
	int 21h

	mov ynansmax, 2                 ; setup maximum length
	mov ah, 0Ah                     ; Request DOS Function 0Ah
	mov dx, OFFSET ynansmax         ; Load offset of string
	int 21h                         ; Buffered Keyboard Input

	xor al, al                      ; emit cr
	xor bx, bx
	mov dx, OFFSET crlftext
	mov ah, 9
	int 21h

	mov bl, ynansr                  ; test answer
	cmp bl, "Y"                     ; "Y", "y" or "1" will quit program
	jz dosquit
	cmp bl, "y"
	jz dosquit
	cmp bl, "1"
	jz dosquit

	xor al, al                      ; emit cr
	xor bx, bx
	mov dx, OFFSET crlftext
	mov ah, 9
	int 21h                         ; no "y" answer -- not quit program
	cmp sp, recoverystkpoint        ; sp popped to after recoverypoint?
	jge restartcode                 ; yes - restart all
	pop bx                          ; no - exit
	jmp bx
dosquit: mov ax, 4c00h
	int 21h

; rootv dir
; [rootv threadc interps sysvar EI top prompt error // R crlf emline token dir xa
; hexo exe hexi fetch adr dump finen crea HL encl pad encl" exitprog Q]

; threadc dir // cp
; [semi reiterate preservetop EIprompt reportemptystack readln emtoken default
; search zabort restart stkcheck threadb does]

; interps dir // xc
; [colon changexp noexe sysvar sysact changexc self text promptext errortext vocab]

; sysvar dir // sv
; [estack prompt lnbfr lnbfcpy adr asmp]

; *****
;       interps colon
; *****
	WORD THIS WORD + 2              ; vocab head
xc01    BYTE 5, "colon", 0              ; xc refers to "execution code"
	WORD OFFSET xc02                ; interpret the data as threaded code
	WORD changexpc
	WORD OFFSET colon
colonext: pop ax        ; 58
colon:  add ax, 2                       ; colon, the threaded list interpreter
	mov bx, ax
	push ax         
	mov cx, colonext
	push cx
	mov ax, [bx]
entert: mov bx, ax                      ; enter threaded list interpreter
	jmp WORD PTR [bx]               ; ax expected to point to link to interpreter


; *****
;       threadc semi
; *****
	WORD THIS WORD + 2              ; vocab head
cp00    BYTE 4, "semi", 0               ; cp refers to "thread code placement"
	WORD OFFSET cp01                ; semi is threaded code return from subroutine
semi    WORD THIS WORD + 2              ; also, "self" will work as an interpreter
semicode: add sp, 4                     ; drop the continuation pointer
next:   pop bx                          ; drop next location
	jmp bx          ; 5B FF E3      ; continue from calling level

; *****
;       interps noexe
; *****
xc02    BYTE 5, "noexe", 0               ; the null interpreter does nothing
	WORD OFFSET xc03                ; on defining a word, it is initially set to
	WORD changexpc                  ; \ the null interpreter
noexe   WORD OFFSET next

; *****
;       interps sysvar
; *****
xc03:
	BYTE 6, "sysvar", 0             ; provide access to system variables
	WORD OFFSET xc04
	WORD changexc
sysvar: add ax, 2                       ; retrieve the value of an internal variable
	mov bx, ax
	mov ax, [bx]                    ; pointer to location follows in thread
	mov [bp], ax
	add bp, 2                       ; push param stack via post increment
	pop bx
	jmp bx

xc04:
	BYTE 6, "sysact", 0     ; system variable at its active position
	WORD OFFSET xc05
	WORD changexc
sysact: add ax, 2
	mov bx, ax
	mov [bp], bx
	add bp, 2
	pop bx
	jmp bx

; *****
;       interps changexc
; *****
xc05:
	BYTE 8, "changexc", 0
	WORD OFFSET xc06                ; this is exactly changexpc
	WORD changexc                   ; but the routine to change
changexc: sub bp, 2                     ; is expected to follow immediately
	mov bx, [bp]                    ; pop param stack via pre-decrement

	add ax, 2                       ; employ with ax pointing to callee location
	mov [bx], ax                    ; change interpreter
	pop bx
	jmp bx

; *****
;       vocab rootv
; *****
en00r: ; initial vocabulary entry _rootv__
	BYTE 5, "rootv", 0               ; this mearly presents the lead vocabulary
	WORD OFFSET en01v               ; \ position 
rootv   WORD sysvar
	WORD THIS WORD + 4              ; simulate a vocabulary entry for "dir"
	WORD OFFSET rootVleadin
	WORD 0 ; in search setting, this would be the executable to interpret
; *****
;       vocab threadc
; *****
en01v:
	BYTE 7, "threadc", 0            ; this is the vocabulary for building words
	WORD offset en02v               ; \ that is, words which cannot be executed
	WORD sysvar                     ; \ immediately effectively.
	WORD OFFSET cp00

; *****
;       vocab interps                   ; this is the vocabulary for interpreter
; *****                                 ; \ routines -- they are set to change any
en02v:                                  ; \ word butil from null initially to the
	BYTE 7, "interps", 0            ; \ indicated interpreter
	WORD offset en03v
	WORD sysvar
	WORD THIS WORD + 4
	WORD OFFSET xc00
	WORD 0

; *****
;       vocab sysvar
; *****
en03v:
	BYTE 6, "sysvar", 0
	WORD offset en04
	WORD sysvar
	WORD OFFSET sv00

; *****
;       estack
; *****
sv00:
	BYTE 6, "estack", 0
	WORD OFFSET sv01
	WORD sysvar ; sysconst
	WORD pstackspace

; *****
;       threadc reiterate
; *****
cp01:
	BYTE 9, "reiterate", 0
	WORD cp02
reiterate WORD this word + 2
	add sp, 6
	pop bx
	sub bx, 2
	push bx
	jmp colonext

; *****
;       EI
; *****
en04:
	byte 2, "EI", 0
	WORD OFFSET en05
	WORD colon
	WORD execinterp                 ; EI is held in this box for re-refrense
	WORD semi                       ; \ via reiterate

execinterp WORD colon
      bo WORD reportemptystack          ; modify prompt if param stack empty
	WORD EIprompt
	WORD token
	WORD rootv
	WORD search
	WORD EIerror                    ; change errormessage to "?"
	WORD zabort                     ; abort if top of stack == 0
	WORD exei                       ; recover previous top of stack and go
	WORD SKerror                    ; change errormessage to "!"
	WORD stkcheck                   ; check stack - abort if underflow
	WORD preservetop                ; hold top of stack for next exe
	WORD reiterate
;        WORD jump
;      ao BYTE OFFSET bo - OFFSET ao - 1

; *****
;       interps self
; *****
xc06:
	BYTE    4, "self", 0            ; self interpreter equivalent to 
	WORD offset xc07                ; \ WORD THIS WORD + 2, easier to program
	WORD changexc                   ; on entry, ax points to calling location
self:   add ax, 2                       ; advance to first word in param list
	mov bx, ax
	jmp bx

; *****
;       top
; *****
en05:
	byte 3, "top", 0                ; recover "overtop" element on stack
	WORD offset en06
top     WORD self
	add bp, 2
	pop bx
	jmp bx

; *****
;       threadc preservetop
; *****
tophold WORD ?
cp02:
	byte 11, "preservetop", 0
	WORD cp03
preservetop WORD self
	mov bx, [bp]
	mov tophold, bx                 ; preserve 'overtop' element on stack
	pop bx                          ; "exei" retrieves this to stack
	jmp bx                          ; \ before executing

; *****
;       threadc EIprompt
; *****
cp03:
	byte 8, "EIprompt", 0
	WORD cp04
EIprompt WORD promptext ;
	BYTE "EI"                       ; see "COMMENT" for promptext interpreter
stackin BYTE " >$"                      ; stack indicator position of prompt

EIerror WORD errortext ;                ; errortext for error reports
	BYTE "? $"

SKerror WORD errortext ;
	BYTE "! $"

; *****
;       threadc reportemptystack
; *****
cp04:
	BYTE 16, "reportemptystack", 0
	WORD OFFSET cp05
reportemptystack WORD   self            ; replace "EI >" with "EI_>", if stack empty
	cmp bp, OFFSET pstackspace      ; this helps user identify stack at floor
	jz emptystacktoreport           ; \ position, where defaults may apply
	mov al, " "
	mov stackin, al
	pop bx
	jmp bx
	  emptystacktoreport:
	mov al, "_"
	mov stackin, al
	pop bx
	jmp bx

; *****
;       interps text
; *****
xc07:
	BYTE 4, "text", 0
	WORD offset xc08
	WORD changexc
text:   add ax, 2                       ; text interpreter
	mov dx, ax                      ; display the bytes following
	mov ah, 9                       ; dos routine strout
	int 21h
	pop bx
	jmp bx

; *****
;       prompt
; *****
promptsize equ 7
en06:
	BYTE 6, "prompt", 0
	WORD OFFSET en07
prompt  WORD text
prompttextposition BYTE "LI >", "$", "$", "$", "$"

; *****
;       error
; *****

en07:
	BYTE 5, "error", 0
	WORD OFFSET en08
	WORD text
errmsgtextposition BYTE "? ", "$", "$", "$", "$", "$", "$",


; *****
;       sysvar prompt
; *****
	WORD THIS WORD + 2              ; vocab head decoration
sv01    BYTE 6, "prompt", 0
	WORD OFFSET sv02
	WORD sysvar
	WORD offset prompttextposition

; *****
;       comment
; *****
en08:
	BYTE 2, "//", 0                 ; comment to EOL
	WORD OFFSET en09                ; ignore rest of line
readnewline WORD colon                  ;
	WORD prompt                     ; also called by token
	WORD readln                     ; when newline is needed
	WORD crlf
	WORD semi                       ; prompt and read line

; *****
;       interps promptext
; *****
xc08:
	BYTE 9, "promptext", 0
	WORD OFFSET xc09
	WORD changexc
promptext:
	add ax, 2
	mov bx, ax                      ; set prompt to prompt-text
	mov di, OFFSET prompttextposition
	mov ch, promptsize              ; max prompt size
	  prompttextnextcharpromptcopy: ; copy ax to prompt
	mov al, BYTE PTR [bx]
	mov [di], al
	cmp al, "$"
	jz promptextendpromptcopy
	cmp ch, 0
	jz promptextendpromptcopy
	sub ch, 1
	add bx, 1
	add di, 1
	jmp prompttextnextcharpromptcopy
	  promptextendpromptcopy:
	pop bx  ; next
	jmp bx

commentprompt WORD promptext
	BYTE "// >$"

; *****
;       interps errortext
; *****
xc09:
	BYTE 9, "errortext", 0
	WORD offset xc10
	WORD changexc
errortext:
	add ax, 2
	mov bx, ax
	mov di, OFFSET errmsgtextposition 
	mov ch, promptsize
	  nextcharerrorcopy:
	mov al, BYTE PTR [bx]
	mov [di], al
	cmp al, "$"
	jz endcharerrcopy
	cmp ch, 0
	jz endcharerrcopy
	sub ch, 1
	add bx, 1
	add di, 1
	jmp prompttextnextcharpromptcopy
	  endcharerrcopy:
	pop bx
	jmp bx

; *****
;       lnbfr
; *****
sv02:
	BYTE 5, "lnbfr", 0
	WORD OFFSET sv02a
	WORD sysvar
	WORD OFFSET inlnbe

inlnsize equ 127
max     BYTE inlnsize                   ; maximum size of input line
inlnbe  BYTE 0
lnbfr   BYTE inlnsize DUP (?)
	BYTE ?                          ; room for " " if buffer full
	BYTE ?                          ; roof for "$" if buffer full
inlncopysize equ 8

lnbfrcopy BYTE  0 
lnbfrcopyst BYTE "$", inlncopysize-2 DUP (?)
					; hold line buffer partial copy for repeats
; *****
;       readlncharpos
; *****
sv02a:
	BYTE 13, "readlncharpos", 0
	WORD OFFSET sv03
	WORD sysact
readlncharpos   BYTE 0                  ; first unexamined char of lnbfr
	BYTE 0

; *****
;       threadc readln
; *****
cp05:
	BYTE 6, "readln", 0
	WORD OFFSET cp06
readln  WORD self
	mov al, inlnbe                  ; do not copy if only a CR
	cmp al, 0
	jz readlnskiplnbfrcopy

	mov di, OFFSET lnbfrcopy
	mov si, OFFSET inlnbe
	mov cx, inlncopysize
	cld
	rep movsb

	  readlnskiplnbfrcopy:
	mov max, inlnsize               ; setup maximum length
	mov ah, 0Ah                     ; Request DOS Function 0Ah
	mov dx, OFFSET max              ; Load offset of string
	int 21h                         ; Buffered Keyboard Input
	mov bl, inlnbe                  ; Put number of chars read in BX
	xor bh, bh                      ; clear the high byte of BX
	mov lnbfr[ bx ], " "            ; " $" terminate string
	add bx, 1
	mov lnbfr[ bx ], "$"            ; for search and echo
	xor bl, bl
	mov readlncharpos, bl           ; clear token position
	mov tokenlength, bl             ; clear token length
	pop bx
	jmp bx

; *****
;       lnbfcpy
; *****
sv03:
	BYTE 7, "lnbfcpy", 0
	WORD OFFSET sv04
	WORD sysvar
	WORD OFFSET lnbfrcopy

; *****
;       R - repeatline
; *****
en09:
	BYTE 1, "R", 0                  ; Repeat: copy over lnbfrpartial copy and redo
	WORD OFFSET en10
	WORD colon
	WORD recopy                     ; "R" only valid as first char of input line
	WORD crlf                       ; \ R is blocked from repeating "R "
	WORD semi                       ; \ Which would be an infinite loop
recopy  WORD THIS WORD + 2
	mov al, lnbfrcopyst+1           ; examine the stored, line to copy back
	cmp al, " "                     ; if second char not space, do not block
	jnz recopynotinfiniteloop
	mov al, lnbfrcopyst
	cmp al, "R"                     ; if also first char space, block
	jz recopyquitinfiniteloop
	  recopynotinfiniteloop:
	mov al, inlnbe                  ; too-long line cannot be restored 
	cmp al, 1 ; inlncopysize        ; this invalidates "R" unless it is only on line
	jz recopylineretrievable
	  recopyquitinfiniteloop:
	add sp, 4                       ; exit recopy (2) and exit "R" (2)
	pop bx
	jmp bx
	  recopylineretrievable:
	xor al, al
	mov tokenbegin, al
	mov tokenlength, al             ; clear token begin and lengths
	mov readlncharpos, al

	mov di, OFFSET inlnbe
	mov si, OFFSET lnbfrcopy
	mov cx, inlncopysize
	cld
	rep movsb

	mov al, inlnbe                  ;
	mov bl, readlncharpos
	mov dx, OFFSET lnbfr
	xor bh, bh
	mov ah, 9                       ; strout
	int 21h

	pop bx
	jmp bx

; *****
;       """ crlf
; *****
en10:
	BYTE 4, "crlf", 0 ; 0277
	WORD OFFSET en12
crlf    WORD text
crlftext BYTE 13, 10, "$"

; *****
;       token
; *****
chdelimiter BYTE " "                    ; space usual delimiter
tokenbegin BYTE 0
tokenlength BYTE 0                      ; TODO cast these into SYSVAR

en12:
	BYTE 5, "token", 0              ; extract next token in input line
	WORD OFFSET en13
token   WORD self
tokencode:      
	mov bl, readlncharpos           ; read number of chars read in BX
	mov dl, inlnbe                  ;   position examined in line
	cmp bl, dl
	jl tokennotAtEOL
	xor bx, bx                      ; EOL found, so
	mov bx, OFFSET tokencode        ; prompt and read in new line
	push bx
	mov ax, OFFSET readnewline      ; enter threaded code executing
	jmp entert                      ; code readnewline.  see "COMMENT" section above
	  tokennotAtEOL:        
	mov bl, readlncharpos           ; parse line for token beginning and length
	xor bh, bh
	mov cl, chdelimiter
	mov dl, lnbfr[ bx ]
	cmp cl, dl
	jnz tokennondelimbegin
	add bl, 1
	mov readlncharpos, bl
	jmp tokencode
	  tokennondelimbegin:
	mov readlncharpos, bl
	mov tokenbegin, bl
	mov ch, 1
	  tokenintoken:
	add bl, 1                       ; added space 
	mov dl, lnbfr[ bx ]
	cmp cl, dl
	jz tokendelimtoend
	add ch, 1
	jmp tokenintoken
	  tokendelimtoend:
	mov tokenlength, ch     
	mov readlncharpos, bl
	pop bx
	jmp bx

; *****
;       threadc emtoken                 ; emit token
; *****
	tokenlenrem BYTE ?
	inlinetokenpos BYTE ?
cp06:
	BYTE 7, "emtoken", 0
	WORD OFFSET cp07
emtoken WORD self
emtokencode:
	mov cl, tokenlength
	mov tokenlenrem, cl
	mov bl, tokenbegin
	mov inlinetokenpos, bl
	  emtokennextcht:
	xor bh, bh
	mov bl, inlinetokenpos
	mov cl, tokenlenrem
	mov al, lnbfr[ bx ]
	cmp cl, 0
	jz coca
	sub cl, 1
	mov tokenlenrem, cl
	add bl, 1
	mov inlinetokenpos, bl
	mov dx, emtokennextcht
	push dx
	jmp coca ;

; *****
;       dir
; *****
en13:
	BYTE 3, "dir", 0
	WORD OFFSET en14
	WORD THIS WORD + 2
	; check stack, if empty, use root vocab by default
	cmp bp, OFFSET pstackspace
	jz diremptystackdira
	sub bp, 2
	mov di, [bp]
      dirpickentrydir:
	sub di, 2
	mov ax, [di]
	mov di, ax

	cmp di, 0
	jnz dirloadeddia                ; valid entry, continue

	mov al, '['                     ; null entry, quit
	mov cx, direndlist
	push cx
	jmp coca

      diremptystackdira:
	mov di, offset rootVdirset
	jmp dirpickentrydir

      dirloadeddia: 
	mov dl, 255 ; limit loop to 255 iterations -
	mov al, '[' ; avoid trap on bad directory call
	mov cx, dirnextentry
	push cx
	jmp coca
      dirnextentry:
	sub dl, 1
	cmp dl, 0
	jz direndlist ; limit loops to 255 iterations

	mov cl, [di]
	inc di
	cmp cl, 0
	jnz dirlistcodechar

	inc di ; handle null token entry without emitting extra space
	mov di, [di]
	cmp di, 0
	jz direndlist
	jmp dirnextentry

      dirlistcodechar: mov al, [di]
	push cx
	mov cx, dir_2a
	push cx
	jmp coca
      dir_2a: pop cx
	inc di
	dec cl
	jnz dirlistcodechar

	inc di ; end of token entry
	mov di, [di]
	cmp di, 0
	jz direndlist
	mov al, " " ; const " " whitespace
	mov cx, dirnextentry
	push cx
	jmp coca
      direndlist:
	mov al, ']'
	mov cx, direndlist2a
	push cx
	jmp coca
      direndlist2a:
	mov ax, offset crlf
	jmp text

  charbufa BYTE "_$"
  coca: 
	mov dx, OFFSET charbufa         ; segment already in DS
	mov ah, 9                       ; load DX with offset of string
	mov charbufa, al
	int 21h                         ; request dos function 9
	pop bx
	jmp bx


; *****
;       threadc default
; *****
cp07:
	BYTE 7, "default", 0
	WORD OFFSET cp08
default: ; if empty stack, execute the next thread, otherwise skip next thread
	WORD self
	cmp bp, OFFSET pstackspace
	jz emptystackdefault
	pop ax
	pop bx
	add bx, 2
	push bx
	mov bx, ax
	jmp bx
	  emptystackdefault:
	pop bx
	jmp bx

; *****
;       xa -- report the execution address of token following
; *****
xaprompt WORD promptext
	BYTE "xa >$"
en14:
	BYTE 2, "xa", 0                 ; retrieve execution
	WORD OFFSET en15                ; \ address of token
	WORD colon                      ; placing token here just executes as alias ?
	WORD xaprompt
	WORD token
	WORD default
	WORD rootv
	WORD search
	WORD zabort
	WORD semi

; *****
;       threadc search                  ; -- includes a second search pre-amble
; *****

cp08:
	BYTE 6, "search", 0
	WORD OFFSET cp09
search  WORD THIS WORD + 2              ; \ execution address, this won't find
	sub bp, 2                       ; \ first entry
	mov di, [bp]
	sub di, 2                       ; uplink precedes execution address
	mov ax, [di]                    ; obtain next link position
	mov di, ax

      searchtokencompare:
	mov si, OFFSET lnbfr
	mov cl, tokenlength
	cmp cl, BYTE PTR [di]
	jnz searchnomatch               ; lengths not matching

	mov bl, tokenbegin
	xor bh, bh
	add si, bx                      ; == si == first char of token
	add di, 1                       ; advance dl to first char in entry
      searchcharcompare:
	cmp BYTE PTR [di], 0            ; found sentinel char
	jz searchmatch

	mov bx, di
	cmpsb
	jnz searchnomatch
	jmp searchcharcompare

      searchnomatch:
	mov di, ax      ; [04] 'e' 'x' 'i' 't' 00 3a 4b
	mov al, BYTE PTR [di]
	add di, 2
	xor ah, ah
	add di, ax  ;  04 'e' 'x' 'i' 't' 00 [3a] 4b
	mov ax, [di]
	cmp ax, 0
	jz serachnotoken

	mov di, ax
	mov ax, di ; hold current dictionary position

	mov si, OFFSET lnbfr
	mov bl, tokenlength
	xor bh, bh
	add si, bx

	mov WORD PTR [bp], di
	jmp searchtokencompare

      serachnotoken:
	mov WORD PTR [bp], 0    ; report failure - return 0
	add bp, 2
	pop bx
	jmp bx

      searchmatch:
	add bx, 4
	mov WORD PTR [bp], bx
	add bp, 2
	pop bx
	jmp bx


; *****
;       threadc zabort
; *****
cp09:
	BYTE 6, "zabort", 0             ; abort line if top of stack empty or zero
	WORD OFFSET cp10                ; - for instance, search leaves 0 on stack
zabort  WORD self                       ; \ if token does not match a line
	cmp bp, OFFSET pstackspace
	jz zaborterrline
	mov bx, [bp-2]                  ; access top of stack without moving stack
	xor ax, ax
	cmp bx, ax
	jz zaborterrline
	pop bx
	jmp bx                          ; all okay, do not abort
      zaborterrline: mov dx, OFFSET errmsgtextposition 
zaborterrout:
	mov ah, 9
	int 21h
	mov bx, offset zaborterrmsg2
	push bx
	jmp emtokencode                 ; echo token
      zaborterrmsg2:
	mov al, inlnbe
	mov readlncharpos, al
	xor ah, ah
	mov tokenbegin, al
	mov tokenlength, ah             ; abort line
	pop bx
	jmp bx                          ; todo? - rewind stack to threaded interp pos.
					; this would require the word "try" to mark
; *****
;       threadc restart
; *****
cp10:
	BYTE 7, "restart", 0            ; rewind all stacks to beginning and reset 
	WORD OFFSET cp11
	WORD self
restartcode: mov bx, recoverystkpoint
	mov sp, bx
	mov ax, exitOrRestartc
	push ax
	mov ax, OFFSET execinterp

	mov ax, OFFSET loopEI
	push ax
	mov cx, OFFSET colonext
	push cx
	mov bx, ax
	mov ax, [bx]
	mov bx, ax
	jmp WORD PTR [bx]

; *****
;       """ hexo
; *****
en15:
	BYTE 4, "hexo", 0               ; emit byte pair in HEX
	WORD OFFSET en16
hexo    WORD OFFSET hexoc
hexmsg  BYTE "FFFF ", "$"
hexlowbyte BYTE 1
hexoc:  mov al, 1
	mov [hexlowbyte], al
	sub bp, 2
	mov bx, [bp]
	mov al, bh
	mov di, OFFSET hexmsg
hexout: mov ah, al
	mov cl, 4
	shr al, cl
	cmp al, 9
	jle num1h
	add al, "A" - 10
	jmp count1h
num1h:  add al, "0"
count1h: mov BYTE PTR [di], al
	add di, 1
	mov al, ah
	and al, 0Fh
	cmp al, 9
	jle num2h
	add al, "A" - 10
	jmp count2h
num2h:  add al, "0"
count2h: mov BYTE PTR [di], al
	add di, 1

	mov al, BYTE PTR [hexlowbyte]
	cmp al, 0
	jz hexoe

	xor al, al  ; mov al, 0
	mov BYTE PTR [hexlowbyte], al
	mov bx, [bp]
	mov al, bl
	jmp hexout
hexoe:
	mov dx, OFFSET hexmsg
	mov ah, 9
	int 21h
	pop bx
	jmp bx

; *****
;       exe
; *****
en16:
	byte 3, "exe", 0
	WORD OFFSET en17
exei    WORD exec
exec:
	sub bp, 2
	cmp bp, OFFSET pstackspace
	jl exeabortactive               ; abort if sub-empty stack now
	mov bx, [bp]
	xor ax, ax
	cmp bx, ax
	jnz exego                       ; also abort if retrieved 0
	  exeabortactive:
	pop bx
	jmp bx
	  exego:        
	mov ax, tophold         ; retrieve previous "overtop" element on stack
	mov [bp], ax
	mov ax, bx                      ; ax points to location of interpreter
	jmp WORD PTR [bx]               ; jump to interpreter of threaded element

; *****
;       threadc stkcheck
; *****
cp11:
	BYTE 8, "stkcheck", 0
	WORD cp12
stkcheck :
	WORD THIS WORD + 2
	cmp bp, OFFSET pstackspace
	jl badstack
	pop bx
	jmp bx
	  badstack:
	mov bp, OFFSET pstackspace      ; reset parameter stack
;       mov dx, OFFSET badstackerrmsg
	jmp zaborterrline ; <== TODO macro here?

; *****
;       hexi
; *****
promptnum WORD promptext
	BYTE "hexi >$"

en17:
	BYTE 4, "hexi", 0
	WORD OFFSET en18
	WORD colon
	WORD promptnum
	WORD token
	WORD hexii
	WORD semi

  tokenbfposloc WORD 0
  ihexibfloc WORD 0
  significantfigurescount BYTE 0
  tokenlenremain BYTE 0
  badnumerrmsg BYTE "# $"

hexii   wORD hexicc
hexicc: xor cx, cx
	mov significantfigurescount, ch ; no signficant figures yet
	mov ihexibfloc, cx              ; clear out ihexifbloc
	mov bx, OFFSET lnbfr
	mov al, tokenlength
	mov tokenlenremain, al
	mov cl, tokenbegin              ; ch remains 0
	add bx, cx
      hexichar:
	mov tokenbfposloc, bx
	mov al, [bx]
	cmp al, '0'
	jl hexi09
	cmp al, '9'
	jg hexi09
	sub al, '0'
	jmp hexiaccum
      hexi09:
	cmp al, 'A'
	jl hexiAF
	cmp al, 'F'
	jg hexiAF
	sub al, 'A'-10
	jmp hexiaccum
      hexiAF:
	cmp al, 'a'
	jl hexiabort
	cmp al, 'f'
	jg hexiabort
	sub al, 'a'-10
      hexiaccum: ;good entry
	mov bx, ihexibfloc
	mov cl, 4
	shl bx, cl
	xor ah, ah
	add bx, ax

	mov ihexibfloc, bx
	mov cl, tokenlenremain
	sub cl, 1
	cmp cl, 0
	jz hexitoken

	mov tokenlenremain, cl
	cmp bx, 0
	jz nosignificantfigure

	mov al, significantfigurescount
	add al, 1
	mov significantfigurescount, al
	cmp al, 4
	jge hexiabort ; number register overflow

	nosignificantfigure: mov bx, tokenbfposloc
	add bx, 1
	jmp hexichar
      hexiabort: ;bad entry
	mov dx, OFFSET badnumerrmsg
	jmp zaborterrout
      hexitoken: ;completed
	mov bx, ihexibfloc
	mov word ptr [bp], bx
	add bp, 2
	pop bx
	jmp bx

; *****
;       fetch
; *****
en18:
	BYTE 5, "fetch", 0              ; TODO unravel handling address and such
	WORD OFFSET en19
	WORD self
	mov bx, [bp-2]
	mov address, bx
	mov ax, [bx]
	mov [bp-2], ax
	pop bx
	jmp bx
address WORD ?

sv04:
	BYTE 3, "adr", 0
	WORD OFFSET sv05
adrpi   WORD sysvar
	WORD OFFSET address

en19:
	BYTE 3, "adr", 0
	WORD OFFSET en20
	WORD colon
	WORD adrpi
	WORD fetchadri
	WORD semi
fetchadri WORD THIS WORD + 2
	mov bx, [bp-2]
	mov ax, [bx]
	mov [bp-2], ax
	pop bx
	jmp bx

; *****
;       dump
; *****
spi     WORD text
	BYTE " $"

en20:
	BYTE 4, "dump", 0
	WORD OFFSET en21
dumpi   WORD dumpc
locat   WORD ?
remln   BYTE ?
dumplinesrem BYTE ?
dumpmsg BYTE "FF $"
dumpc:
	cmp bp, OFFSET pstackspace
	jnz dumpnotemptystack
	add bp, 2                       ; on empty stack, execute a _TOP_ func.
dumpnotemptystack:
	mov bx, [bp-2]
	mov locat, bx
	mov al, 8
	mov dumplinesrem, al

dumplinesloop:
	mov al, 16
	mov remln, al
	mov ax, dumplinehead1
	push ax
	jmp hexoc

dumplinehead1:
	mov ax, dumplinehead2
	push ax
	mov ax, OFFSET spi
	jmp text
dumplinehead2:
	mov bx, locat
dumploop:
	mov di, OFFSET dumpmsg
	mov al, BYTE PTR[bx]
	add bx, 1
hexdout: mov ah, al
	mov cl, 4
	shr al, cl
	cmp al, 9
	jle numd1h
	add al, "A" - 10
	jmp countd1h
numd1h: add al, "0"
countd1h: mov BYTE PTR [di], al
	add di, 1
	mov al, ah
	and al, 0Fh
	cmp al, 9
	jle numd2h
	add al, "A" - 10
	jmp countd2h
numd2h: add al, "0"
countd2h: mov BYTE PTR [di], al
dumpout: mov dx, OFFSET dumpmsg
	mov ah, 9
	int 21h
	mov al, remln
	sub al, 1
	mov remln, al
	cmp al, 0
	jz dumpAF
	cmp al, 8
	jz inlinedash
	jmp dumploop
linedash BYTE "- $"
inlinedash:
	mov dx, OFFSET linedash
	mov ah, 9
	int 21h
	jmp dumploop

charmsg BYTE " $"

dumpAF:
	mov bx, locat
	mov al, 16
	mov remln, al
	
	  dumpcharloop:
	mov bx, locat
	mov cl, BYTE PTR[bx]
	add bx, 1
	mov locat, bx
	cmp cl, " "
	jl ctrlchar
	cmp cl, "~"
	jg ctrlchar
	jmp dumpcharout
	ctrlchar: mov cl, "."
	dumpcharout:
	mov charmsg, cl
	mov dx, OFFSET charmsg
	mov ah, 9
	int 21h

	mov al, remln
	sub al, 1
	mov remln, al
	cmp al, 0
	jz dumpcharexit
	jmp dumpcharloop

	  dumpcharexit:
	mov ax, dumplinesloopset
	push ax
	mov ax, OFFSET crlf
	jmp text

	  dumplinesloopset:
	mov al, dumplinesrem
	sub al, 1
	mov dumplinesrem, al
	cmp al, 0
	jz dumproutinexit

	mov bx, locat
	mov [bp], bx
	add bp, 2

	jmp dumplinesloop

	  dumproutinexit:
	mov bx, [bp]            ; adjust top of stack to next dump location
	add bx, 10h
	mov [bp], bx
	pop bx
	jmp bx

; *****
;       finen                           ; report interp position at vocabulary end
; *****
en21:
	BYTE 5, "finen", 0
	WORD OFFSET en22
finen   WORD THIS WORD + 2
	cmp bp, OFFSET pstackspace
	jz finenemptystack
	sub bp, 2
	mov bx, [bp]
	sub bx, 2                       ; retreat to link position
	mov ax, [bx]
	cmp ax, 0                       ; allow for null vocabulary, initial test
	jz finenadv
	mov bx, ax
;       jmp finenext

	  finenext:
	mov al, byte ptr [bx]
	xor ah, ah
	add bx, ax
	add bx, 2

	mov ax, [bx]
	cmp ax, 0
	jz finenadv
	mov bx, ax
	jmp finenext

	  finenemptystack:
	mov bx, OFFSET en00             ; root vocab default
	jmp finenext

	  finenadv:
	add     bx, 2                   ; advance to interpreter position
	mov [bp], bx            ; last entry
	add bp, 2
	pop bx
	jmp bx

; *****
;       sysvar asmp - active var
; *****
sv05:
	BYTE 4, "asmp", 0
	WORD 00
	WORD sysact
asmpos  WORD OFFSET assembletohere      ; initially set to this point, but
					; this advances on "encl"
; *****
;       crea
; *****
creaprompt WORD promptext
	BYTE "crea >$"

en22:
	BYTE 4, "crea", 0
	WORD OFFSET en23
crea    WORD colon
	WORD default
	WORD rootv
	WORD finen
	WORD creaprompt
	WORD token
	  WORD emtoken ; TODO check that word entered is unique?
	WORD placetoken
	WORD semi
placetoken WORD THIS WORD + 2
	sub bp, 2                       ; place link
	mov bx, [bp]
	sub bx, 2                       ; retreat to link position
	mov ax, asmpos
	mov [bx], ax
	; here link is placed, now place token

	mov bx, ax
	mov dh, tokenlength
	mov byte ptr [bx], dh

	add bx, 1
	mov asmpos, bx ; placed token length, now place token char's

	mov cx, OFFSET lnbfr
	mov di, cx
	mov al, tokenbegin
	xor ah, ah
	add di, ax
	  placenexttokenchar: 
	mov cl, BYTE PTR [di]
	mov [bx], cl

	add bx, 1
	add di, 1
	sub dh, 1
	cmp dh, 0

	jz placetokenend
	jmp placenexttokenchar ; -- verified, places first char of token

	  placetokenend:
	xor ax, ax
	mov [bx], al    ; 00 byte
	add bx, 1
	mov [bx], ax    ; 00 word
	add bx, 2
	mov ax, bx
	add ax, 2
	mov [bx], OFFSET next                   ; null-pointer
	add bx, 2
	mov asmpos, bx

	pop bx
	jmp bx

; *****
;       HL
; *****
promptHL WORD promptext
	BYTE "HL >$"
en23:
	BYTE 2, "HL", 0                 ; hex loader code
	WORD OFFSET en24
	WORD OFFSET hlc

hilo    BYTE 0                          ; flag: contains a 1 if read halfbyte
hiread  BYTE ?                          ; first char read leftshifted 4

hlc:
HLpromptin:
; display prompt and read line
	mov bx, OFFSET hl00
	push bx
	mov ax, OFFSET promptHL
	jmp entert
	  HL00:
	mov bx, OFFSET HL01             ; ignores rest of line, refreshes new
	push bx
	mov ax, OFFSET readnewline
	jmp entert                      ; enter threaded code for prompt, echo
	  HL01:
	xor al, al                      ; clear al
	mov readlncharpos, al           ; set to examine first char of input
	mov bl, inlnbe
	cmp bl, 1
	jnz HLnotonecharcmd
	mov al, lnbfr
	cmp al, "Q"                     ; quit if entering "Q" first char
	jz HLfinish

	  HLnotonecharcmd:
; examine each char of input
	xor bx, bx                      ; clear bx

	  HLnextcharinput:
	cmp bl, inlnbe
	jz HLpromptin
	xor ah, ah                      ; clear ah
	mov al, lnbfr[ bx ]

	cmp al, "/"                     ; comment?
	jnz HLnotcomment
	add bx, 1                       ; lookahead one character
	cmp bl, inlnbe
	jz HLpromptin
	mov al, lnbfr[ bx ]
	cmp al, "/"                     ; comment token "//" to end of line?
	jz HLpromptin                   ; detected comment, ignore to EOL
	sub bx, 1                       ; recycle the lookahead char read

	  HLnotcomment:
	cmp al, "A"
	jge HLcharAB

	cmp al, "0"
	jge HLchar01
	add bx, 1
	jmp HLnextcharinput             ; not recognized - ignore

	  HLchar01:
	cmp al, "9"
	jle HLchar09
	add bx, 1
	jmp HLnextcharinput
		
	 HLchar09:
	sub al, "0"                     ; "0" <= ch <= "9"
	jmp HLplacenum

	  HLcharAB:
	cmp al, "F"
	jle HLcharAF
	add bx, 1
	jmp HLnextcharinput

	  HLcharAF:
	sub al, "A" - 10                ; "A" <= ch <= "F"

	  HLplacenum:
	mov dl, hilo
	cmp dl, 1
	jz HLplacepair

	mov dl, 1
	mov hilo, dl
	mov cl, 4
	shl ax, cl
	mov hiread, al
	add bx, 1
	jmp HLnextcharinput

	  HLplacepair:
	mov dl, 0
	mov hilo, dl
	mov cx, bx
	mov bx, asmpos

	; xor   ah, ah                  ; clear ah
	or al, hiread
	mov [bx], al
	add bx, 1
	mov asmpos, bx
	mov bx, cx
	add bx, 1
	jmp HLnextcharinput

	  HLfinish:     
	mov readlncharpos, 1    ; do not reparse the "Q"
	mov al, hilo
	cmp al, 0
	jz HLquitroutine

; a partial pair rests in hiread, logic_or it with asmpos location
; assert last partial pair but do not increment address
	mov bx, asmpos
	mov al, [bx]
	and al, 0Fh
	or al, hiread
	mov [bx], al

	  HLquitroutine:
	pop bx
	jmp bx

; *****
;       encl
; *****
en24:
	BYTE 4, "encl", 0               ; enclose word to asmp and update asmp
	WORD OFFSET en25
encl    WORD THIS WORD + 2
	sub bp, 2
	mov ax, [bp]
	cmp ax, 0                       ; 0 is blocked from enclosing as it is the
	jz enclerror                    ; \ result of not finding an entry
	mov bx, asmpos
	mov [bx], ax                    ; if 0 is needed to place, use "pad", below
	add bx, 2
	mov asmpos, bx
	pop bx
	jmp bx
	  enclerror:
	pop bx
	jmp bx

; *****
;       pad
; *****
en25:
	BYTE 3, "pad", 0 ; places a single word 00 at asmpos and advances
	WORD OFFSET en26
	WORD self
	mov bx, asmpos
	xor ax, ax
	mov [bx], ax
	add bx, 2
	mov asmpos, bx
	pop bx
	jmp bx

; *****
;       encl"
; *****
en26:
	BYTE 5, "encl", 22h, 0  ; enclose doublequoted string
	WORD OFFSET en27
	WORD colon
	WORD delimquote
	WORD token
	WORD delimspace
	WORD emtoken
	WORD crlf
	WORD encltoken
	WORD semi
	  delimquote WORD self
	mov al, 22h
	mov chdelimiter, al
	pop bx
	jmp bx
	  delimspace WORD self
	mov al, 20h
	mov chdelimiter, al
	mov al, tokenlength
	sub al, 1
	mov tokenlength, al

	mov ah, readlncharpos
	add ah, 1
	mov readlncharpos, ah

	pop bx
	jmp bx
tokenencrem BYTE ?
tokenencpos BYTE ?
encltoken WORD self
	mov cl, tokenlength
	mov tokenencrem, cl
	mov bl, tokenbegin
	add bl, 1                       ; skip space beginning token
	mov tokenencpos, bl
	  nextencch:
	xor bh, bh
	mov bl, tokenencpos
	mov cl, tokenencrem
	mov al, lnbfr[ bx ]
	cmp cl, 0
	jz endencch
	sub cl, 1
	mov tokenencrem, cl
	add bl, 1
	mov tokenencpos, bl
	mov bx, asmpos
	mov BYTE PTR [bx], al
	add bx, 1
	mov asmpos, bx
	jmp nextencch
	  endencch:
	pop bx
	jmp bx

; *****
;       threadc threadb
; *****
cp12:
	BYTE 7, "threadb", 0
	WORD cp13
threadb WORD this word + 2 ;retrieve next byte to stack as signed-byte
	add sp, 2
	pop bx
	add bx, 2
	mov ah, 0
	mov al, [bx]
	test al, 80h                    ; any byte larger than 80h
	jz posthreadb                   ; is treated as a negative
	  not ah                        ; equivalent to CBW
	posthreadb:  mov word ptr [bp], ax
	add bp, 2
	dec bx ;location is set one higher than expected, usu. word forms
	push bx
	jmp colonext

; *****
;       does
; *****
cp13    BYTE 4, "does", 0
	WORD 0
does    WORD colon              ; DDA
	WORD rootv
	WORD finen
	WORD doesii
	WORD top                        ; try this - works
;       WORD rootv                      ; this can also be accomplished with a "dup"
;       WORD finen
	WORD placeposexit               ; also exits this routine

placeposexit WORD THIS WORD + 2 ; calculate threadpos of parent and enclose over doespad
	mov bx, sp
	add bx, 6                       ; retain the calling location to enclose
	mov ax, [bx]

	sub bp, 2
	mov bx, [bp]
	add bx, 2
	
	mov [bx], ax

	add sp, 8                       ; exit this scope as well as calling scope
	pop bx                          ; \ which is _const_.
	jmp bx

doesii  WORD changexc
doesc:  mov cx, ax
	add cx, 4                       ; advance to object position
	mov [bp], cx
	add bp, 2                       ; save object position to stack
	add ax, 2                       ; retreat to threadc rejoin point
	mov bx, ax
	mov ax, [bx]                    ; see threadc rejoin point
	jmp colon

; *****
;       interp vocab
; *****
xc10:
	BYTE 5, "vocab", 0              ; set vocabulary head interpreter
	WORD 0
WORD    changexc                        ; TODO - additional setup assembly here?
	add ax, 6                       ; pass interpreter (the pointer to this routine)
	mov [bp], ax                    ; store loc to stack
	add bp, 2                       ; \ one after vocab link
	pop bx
	jmp bx

; *****
;       exitprog
; *****

en27:
	BYTE 8, "exitprog", 0
	WORD OFFSET enEND
	WORD exitOrRestartc

; *****
;       Q
; ***** 

enEND:
	BYTE 1, "Q", 0          ; it might be nice only to activate "Q" if
	WORD 00 ; dict. final entry  ; \ only character on line like HL
	WORD OFFSET semicode            ; exit "fallout" routine (end program)
assemblefromhere:
END

colon           the threaded code (T-code) interpreter
changexp        xc - change the interpreter to the labeled point in the program
  semi          T-code return
noexe            ignore data, no-operation
sysvar          xc - return a pointer to the system variable following
sysact          xc - actual variable location follows
changexc        xc - change the interpreter to the following machine code
. rootv         en99 - root vocabulary - all word must be attached to here      
. threadc       cp99 - thread-code vocabulary - routines that won't execute directly
. interps       xc99 - interpreters vocabulary - routines for interpreting labeled data
. sysvar        sv99 - system variables vocabulary
    estack      sv - location of parameter stack pointer (BR) when stack is empty
  reiterate     rewind and re-enter the current thread code list from the beginning
. EI            the execution interpreter, the fundimental loop in the program
self            xc - program immediately follows, and is native code (self-interpreted)
. top           push the item last on the top of the stack - does not pop the stack
  preservetop   hold the top of the stack element for possible retrieval later
  EIprompt      change the prompt to the executable interpreter's prompt
  reportemptystack change the prompt to indicate that the parameter stack is empty
text            xc - display the text following (bytes) to the console
. prompt        display the current data-input prompt
. error         display the current erroneous input indicator
    prompt      sv - show the location where the system prompt is stored
. //            comment, ignore characters following to the end of the line
promptext       xc - change the system prompt to match the text following
errortext       xc - change the system error indicator to match the text following
    lnbfr       sv - the location for the line buffer in its entirety
  readln        cp - read in a new line
    lnbfcpy     sv - the location copying the first few chars of lnbfr for retrieval
. R             restore and reexecute previous input line, R must be first char
. crlf          output a carriage return linefeed to the terminal
. token         extract a token from the input line, will prompt if line empty
  emtoken       cp - emit the token read in to the console
. dir           display all items on the vocabulary, from the dos command dir(ectory)
  default       cp - if the stack is empty, do the routine following, else skip follow
. xa            return the execution point matching token following or 0 if not found
  search        cp - find the execution interpreter matching the token following
  zabort        cp - if the stack is empty or zero, abort the rest of the line
  restart       rewind the return stack to the initial point and begin program again
. hexo          output the number on the stack in hexidecimal
. exe           execute the execution point entry on the stack
  stkcheck      check the stack, if empty, issue an error and abort the rest of the line
. hexi          read the next token and interpret it as a hexidecimal number
. fetch         place top of stack to variable adr, return mem[ adr ]
    adr         sv - the address last employed for a fetch
. adr           the last address employed for a fetch
. dump          display memory according to the location on the stack
. finen         return the exec point of final entry on the vocabulary (default rootv)
    asmp        sv - assembly point, all enclosures copy here, then advances
. crea          build a noexe labeled routine attached to the vocabulary (default rootv)
. HL            (HEX LOADER) enclose to the assembly point the hex code entered
. encl          enclose the bytepair on the stack to the assembly point, not 00
. pad           enclose the word 0 to the assembly point
. encl"		read the token following with " as a delimiter, and enclose the token
threadb         cp - literal byte follows in thread
does            cp - threadcode interpreter follows for run-time behaivoir
vocab           xc - the word defines the head of a vocabulary
. exitprog      query the user, if affirmative, terminate the execution of the program
		- otherwise, restart program.
. Q             drop the routine currently executing -- if "bottom", exitprog activates

jump    WORD this word + 2 ; jump distance given by
	add sp, 2       ; \ byte following in threaded call list
	pop bx
	add bx, 2
	xor ah, ah
	mov al, [bx]
	test al, 80h
	jz posjzb2
	  mov ah, 0ffh
	posjzb2:
	add bx, ax

	sub bx, 1
	push bx
	jmp colonext

jzb:
	WORD this word + 2 ;if top of stack is 0, jump back by byte following
	add sp, 2
	pop bx
	add bx, 2
	mov ah, 0
	mov al, [bx]
	test al, 80h
	jz posjzb
	  mov ah, 0ffh
	posjzb:

	sub bp, 2 ;parameter stack
	mov cx, [bp]

	xor dx, dx
	cmp cx, dx
	jnz endthreadb
	add bx, ax

      endthreadb:
	dec bx
	push bx
	jmp colonext

//threadc crea threadb HL // this currently fails for unknown reason
//83C402  //    ADD     SP,+02 ; next byte as sbyte to stack
//5B      //    POP     BX
//83C302  //    ADD     BX,+02
//B400    //    MOV     AH,00
//8A07    //    MOV     AL,[BX]
//A880    //    TEST    AL,80
//7402    //    JZ      0ADC
//F6D4    //    NOT     AH
//894600  //    MOV     [BP+00],AX
//83C502  //    ADD     BP,+02
//4B      //    DEC     BX
//53      //    PUSH    BX
//58      //    POP     AX     ; colonext
//83C002  //    ADD     AX,+02 ; colon
//8BD8    //    MOV     BX,AX
//50      //    PUSH    AX
//B92601  //    MOV     CX,0126
//51      //    PUSH    CX
//8B07    //    MOV     AX,[BX]
//8BD8    //    MOV     BX,AX  ; entert
//FF27    //    JMP     [BX]
//Q
//threadc finen    interps xa self exe

//  B4 0C 83 C4 02 5B 83 C3 - 02 B4 00 8A 07 A8 80 74 .....[.........t
//  02 F6 D4 89 46 00 83 C5 - 02 4B 53 E9 B7 F4 08    ....F....KS....

; ***** BEGIN included parts
; *****
; *****

// ***** ARITHMETIC and LOGIC

rootv crea add HL
 83ED02 //     SUB     BP,+02
 8B5E00 //     MOV     BX,[BP+00]
 8B46FE //     MOV     AX,[BP-02]
 03C3   //     ADD     AX,BX
 8946FE //     MOV     [BP-02],AX
 5B     //     POP     BX
 FFE3   //     JMP     BX
Q
rootv xa add  interps xa self exe

rootv crea sub HL
 83ED02 //     SUB     BP,+02           ; pop, pre sub 2
 8B5E00 //     MOV     BX,[BP+00]
 8B46FE //     MOV     AX,[BP-02]
 2BC3   //     SUB     AX,BX
 8946FE //     MOV     [BP-02],AX
 5B     //     POP     BX
 FFE3   //     JMP     BX
Q
xa sub  interps xa self exe

rootv crea and HL
 83ED02 //     SUB     BP,+02
 8B5E00 //     MOV     BX,[BP+00]
 8B46FE //     MOV     AX,[BP-02]
 23C3   //     AND     AX,BX
 8946FE //     MOV     [BP-02],AX
 5B     //     POP     BX
 FFE3   //     JMP     BX
Q
rootv finen   interps xa self exe

rootv crea or HL
 83ED02 //     SUB     BP,+02
 8B5E00 //     MOV     BX,[BP+00]
 8B46FE //     MOV     AX,[BP-02]
 0BC3   //     OR      AX,BX
 8946FE //     MOV     [BP-02],AX
 5B     //     POP     BX
 FFE3   //     JMP     BX
Q
finen   interps xa self exe

rootv crea xor HL
 83ED02 //     SUB     BP,+02
 8B5E00 //     MOV     BX,[BP+00]
 8B46FE //     MOV     AX,[BP-02]
 33C3   //     XOR     AX,BX
 8946FE //     MOV     [BP-02],AX
 5B     //     POP     BX
 FFE3   //     JMP     BX
Q
finen   interps xa self exe

rootv crea not HL
 8B5EFE //     MOV     BX,[BP-02]
 F7D3   //     NOT     BX
 895EFE //     MOV     [BP-02],BX
 5B     //     POP     BX
 FFE3   //     JMP     BX
Q
finen   interps xa self exe

rootv crea shl HL
 8B5EFE //     MOV     BX,[BP-02]
 D1E3   //     SHL     BX,1
 895EFE //     MOV     [BP-02],BX
 5B     //     POP     BX
 FFE3   //     JMP     BX
Q
finen   interps xa self exe

rootv crea shr HL
 8B5EFE //     MOV     BX,[BP-02]
 D1EB   //     SHR     BX,1
 895EFE //     MOV     [BP-02],BX
 5B     //     POP     BX
 FFE3   //     JMP     BX
Q
finen   interps xa self exe

// ***** MEMORY

rootv crea poke HL
83ED02  //      SUB     BP,+02
8B5E00  //      MOV     BX,[BP+00]
83ED02  //      SUB     BP,+02
8B4600  //      MOV     AX,[BP+00]
8907    //      MOV     [BX],AX
5B      //      POP     BX
FFE3    //      JMP     BX
Q
finen    interps xa self exe

// ***** STACK

rootv crea dup HL
8B46FE //       MOV     AX,[BP-02]
894600 //       MOV     [BP+00],AX
83C502 //       ADD     BP,+02
5B     //       POP     BX
FFE3   //       JMP     BX
Q
finen     interps xa self exe

rootv crea drop HL
83ED02 //       SUB     BP,+02
5B     //       POP     BX
FFE3   //       JMP     BX
Q
finen      interps xa self exe

rootv crea swap HL
8B46FC //       MOV     AX,[BP-04]
8B5EFE //       MOV     BX,[BP-02]
8946FE //       MOV     [BP-02],AX
895EFC //       MOV     [BP-04],BX
5B     //       POP     BX
FFE3   //       JMP     BX
Q
finen      interps xa self exe

// pick ( nm ... n2 n1 k -- nm ... n2 n1 nk )
// push a copy of the n-th item on parameter stack.
rootv crea pick HL
8B5EFE //       MOV     BX,[BP-02]
D1E3   //       SHL     BX,1
8BFD   //       MOV     DI,BP
2BFB   //       SUB     DI,BX
83EF02 //       SUB     DI,+02
8B1D   //       MOV     BX,[DI]
895EFE //       MOV     [BP-02],BX
5B     //       POP     BX
FFE3   //       JMP     BX
Q
finen      interps xa self exe

// *****
// ***** internal and tests
// *****

// ***** constp -- constand and print

crea constp
xa rootv encl  xa crea encl  xa pad encl  xa encl encl  
  threadc xa does encl
xa fetch encl  xa hexo encl  xa crlf encl  threadc xa semi encl
finen  interps xa colon exe

hexi cafe constp cafep

rootv crea rspp HL // return stack pointer report
8BC4   //     MOV     AX,SP
894600 //     MOV     [BP+00],AX
83C502 //     ADD     BP,+02
5B     //     POP     BX
FFE3   //     JMP     BX
Q
finen     interps xa self exe

// ***** threadb test

rootv crea one
threadc xa threadb encl
HL
01
Q
threadc xa semi encl
rootv finen interps xa colon exe

// this was employed to help test the R, repeat last line, command
crea l  
sysvar xa lnbfr encl  xa dump encl  xa dump encl  xa crlf encl
threadc xa semi encl
xa l  interps xa colon exe


// ***** emline

rootv crea emln HL
83ED02 //       SUB     BP,+02
8B5E00 //       MOV     BX,[BP+00]
8A0F   //       MOV     CL,[BX]
83ED02 //       SUB     BP,+02
8B5600 //       MOV     DX,[BP+00]
8BDA   //       MOV     BX,DX
83EB01 //       SUB     BX,+01
8A07   //       MOV     AL,[BX]
32ED   //       XOR     CH,CH
03D1   //       ADD     DX,CX
83C201 //       ADD     DX,+01
B409   //       MOV     AH,09
CD21   //       INT     21
5B     //       POP     BX
FFE3   //       JMP     BX
Q
rootv finen   interps xa self exe

rootv crea emline
sysvar xa lnbfr encl
sysvar xa readlncharpos encl
rootv xa emln encl
threadc xa semi encl
rootv finen    interps xa colon exe

crea //p // repeat the comment, was native code
xa emline encl
xa crlf encl
xa prompt encl
threadc xa readln encl
xa crlf encl
threadc xa semi encl
finen interps xa colon exe


// ***** dumpit

crea dafromxa HL ; retreat to text defining execution point
 83ED02 //       SUB     BP,+02
 8B5E00 //       MOV     BX,[BP+00]
 83EB03 //       SUB     BX,+03 ; prev word, downlink
 8A07   //       MOV     AL,[BX] ; check that this is senteniel
 32C9   //       XOR     CL,CL
 38C8   //       CMP     AL,CL
 7517   //       JNZ     0831 ; to emptystackorothererror
// 081A: nextpossiblechar:
 83EB01 //       SUB     BX,+01
 8A07   //       MOV     AL,[BX]
 38C8   //       CMP     AL,CL
 7405   //       JZ      0828 ; to foundbeginning
 80C101 //       ADD     CL,01
 EBF2   //      JMP     081A ; to nextpossbilechar
// 0828: foundbeginning:
 895E00 //       MOV     [BP+00],BX ;
 83C502 //       ADD     BP,+02
 5B     //       POP     BX
 FFE3   //       JMP     BX
// 0831: emptystackorothererror:
 33DB   //       XOR     BX,BX
 F7D3   //       NOT     BX
 895E00 //       MOV     [BP+00],BX
 5B     //       POP     BX
 FFE3   //       JMP     BX
Q
finen  interps xa self exe

crea dumpit
xa token encl // TODO error - entered tab not counted as space
threadc xa search encl // routine untab, that expands tabs into spaces?
threadc xa zabort encl
xa dafromxa encl
xa dump encl
threadc xa semi encl
xa dumpit interps xa colon exe
//
// rootv dumpit dumpit // example use
// rootv dumpit rootv

// ***** xafromda

// ; advance to execution point from directory
rootv crea xafromda HL
83ED02 //       SUB     BP,+02
8B5E00 //       MOV     BX,[BP+00]
8A07   //       MOV     AL,[BX]
32E4   //       XOR     AH,AH
03D8   //       ADD     BX,AX
83C301 //       ADD     BX,+01
8A07   //       MOV     AL,[BX]
3C00   //       CMP     AL,00
750C   //       JNZ     0842
83C303 //       ADD     BX,+03
895E00 //       MOV     [BP+00],BX
83C502 //       ADD     BP,+02
5B     //       POP     BX
FFE3   //       JMP     BX
33DB   //       XOR     BX,BX // @842
F7D3   //       NOT     BX
895E00 //       MOV     [BP+00],BX
5B     //       POP     BX
FFE3   //       JMP     BX
Q
finen        interps xa self exe

// ***** message

crea m2
encl" Message goes here. $"
xa m2  interps xa text exe

// ***** promptest

threadc crea oddprompt encl" @@ >$$" 
threadc finen interps xa promptext exe
//
crea prtest
threadc xa oddprompt encl
xa emline encl
xa crlf encl
xa // encl
xa emline encl
xa crlf encl
threadc xa EIprompt encl
xa // encl
threadc xa semi encl
finen   interps xa colon exe

// ***** new vocabulary

rootv crea linkpad HL // this lays a pointer to 0 and advances asmp
83ED02 //       SUB     BP,+02
8B7600 //       MOV     SI,[BP+00]
8BDE   //       MOV     BX,SI
8B1F   //       MOV     BX,[BX] ; retrieve assembly position
8BC3   //       MOV     AX,BX
83C002 //       ADD     AX,+02
8907   //       MOV     [BX],AX
83C302 //       ADD     BX,+02
33C0   //       XOR     AX,AX
8907   //       MOV     [BX],AX
83C302 //       ADD     BX,+02
891C   //       MOV     [SI],BX ; save new assmbly position
5B     //       POP     BX
FFE3   //       JMP     BX
Q
rootv finen  interps xa self exe

rootv crea vt  sysvar xa asmp exe  linkpad
rootv finen  interps xa vocab exe

vt dir
vt crea firstinvt
vt dir

// ***** STACK DEPTH

crea sdoffset HL // stack depth offset
8B5EFE //        MOV     BX,[BP-02]
8BC5   //        MOV     AX,BP
2BC3   //        SUB     AX,BX
83E802 //        SUB     AX,+02
D1E8   //        SHR     AX,1
8946FE //        MOV     [BP-02],AX
5B     //        POP     BX
FFE3   //        JMP     BX
Q
finen  interps xa self exe

crea sdepth // report stack depth
sysvar xa estack encl
rootv xa sdoffset encl
threadc xa semi encl
rootv finen  interps xa colon exe
