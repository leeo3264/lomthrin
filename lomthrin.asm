lomthrin SEGMENT PARA READ WRITE EXECUTE
	includelib kernel32.lib  ; PARA indicates segment is alligned to 16-byte boundary (paragraph)
	; EXTERN AddVectoredExceptionHandler : PROC    ; EXTERN SetUnhandledExceptionFilter : PROC
	lomthrin_fn PROC  ; entry point from source file stub main.cpp
	msca_entire equ 7fh ; sectiontype indicator code: entire memory section allocation

entrypoint: jmp startupInitialize ; eb 7a
	byte 6, "mthrin", 0, msca_entire
	db 2 ; per msca_entire, in one byte here, indicate quantity of bytes following employed for displaying allocation amount
	dw offset RuntimeBuiltCodesBegin - this word ; allocation amount

msca1 equ 01h ; sectiontype indicator code: memory section allocated continuous of less than 256 bytes followed by bytes of lead data
dq 3013EE061854CA78h
byte 15, "CCholdIOhandles", 0, msca1 ;    C C h o l d I O h a n d l e s
db offset endCCholdIOhandles - this byte, endCCholdIOhandles - this byte ; section size, employed values before threaded header
	holdgivenRSP dq ? ; RCX RDX R8 R9 contain the first four arguments; remaining are on the stack
	holdgivenRBP dq ? ; RBX-0 RBP-v RDI-0 RSI-0 R12-0 R13-0 R14-0 R15-0 ; calling convention request these be restored
	returnstackbottom dq ? ; hold address of SP for bottom return address
	parameterstackbottom dq ? ; hold address for BP for empty parameter stack

	; hold values from console input and output
	extern GetStdHandle: PROC
	ConsoleIncd equ -10
	ConsoleInHandle qword ?
	ConsoleOutcd equ -11
	ConsoleOutHandle qword ?

	; hold values for exception handler
	;oldSEH dq ?
	;ExceptionHandler dq ?
	  endCCholdIOhandles:

byte 17, "startupInitialize", 0, msca1 ;    s t a r t u p I n i t i a l i z e
db offset endstartupinitialize - this byte, endstartupinitialize - this byte ; does not have a threaded code name call
startupInitialize:
	mov holdgivenRSP, RSP
	sub RSP, 64 ;  provide underflow buffer of 8 quadwords
	xor RAX, RAX
	mov RAX, 0BAD1h ;  give underflow warning value
	push RAX ;
	add RSP, 8
	mov holdgivenRBP, RBP
	mov RBP, RSP
	mov parameterstackbottom, RSP
	sub RSP, 1000h ; allocate 1K for parameter stack frame
	mov returnstackbottom, RSP

	mov rax, offset RuntimeBuiltCodesBegin ; location at end of this code for adding additional routines
	mov asmpos, rax ; save to system var asmpos
	;
	mov rsi, rcx ; // copy string passed from rcx, format and copy into inlinebf
	mov rdi, rcx ; pointer to string passed by way of procedure from c++ stub
	mov rcx, inlnsize ; max inline size scan region
	mov al, 00h ; match 00h in search, string is null terinated
	repne scasb ; search byte forward
	jz found_null  ; jump if null byte found
	xor rcx, rcx ; not found, zero out rcx
	jmp not_found  ; jump if null byte not found sets rcx to 1
	  found_null: mov rbx, rdi
	sub rbx, rsi ; rbx contains length of string + 1
	mov rcx, offset lnbf_numchars
	mov byte ptr [rcx], bl ; save num chars+1
	mov rcx, rbx
	shr rcx, 3 ; divide by 8
	  not_found: add rcx, 1 ; add 1 to include final partial quadword
	mov rdi, offset inlnbf
	rep movsq
	mov rcx, offset inlnbf
	add rcx, rbx
	sub rcx, 1
	mov byte ptr [rcx], 20h ; insert spc and newline characters -- also insert line character count
	add rcx, 1
	mov byte ptr [rcx], 0dh ; newline

 ; set up handles for operating system calls, mostly for input/output
	sub rsp, 48h ; leave shadow stack working space, as required per calling convention
	mov rsi, rcx ; preserve initial string passed from c++ stub, usually interpreter
	mov rcx, ConsoleOutcd
	call GetStdHandle
	mov ConsoleOutHandle, rax
	mov rcx, ConsoleIncd
	call GetStdHandle
	mov ConsoleInHandle, rax
	add rsp, 48h ;

 ; set up structured exception handler
 ; mov rcx, offset SEH_catch
 ; call SetUnhandledExceptionFilter
 ; call AddVectoredExceptionHandler

 ; overwrite two byte jump code at entrypoint and expand section header from "omthrin" to "lomthrin"
	mov rdi, offset entrypoint
	add rdi, 2
	mov al, byte ptr [rdi]
	add al, 2
	sub rdi, 2
	mov [rdi], al
	add rdi, 1
	mov al, 'l'
	mov [rdi], al
	add rdi, 1
	mov al, 'o'
	mov [rdi], al
	
	jmp restartic
	  endstartupInitialize:

; vocabularies: rootv threadc interps -- token-words: estack adr encl pad threadb does
byte 5, "rootv", 0, msca1 ;   r o o t v    sc00 -> sc01
db offset endrootv - this byte, sc00 - this byte
    dq 11D7D34C56431F2Ch
	rootvname byte 5, "rootv", 0 ;
	dq offset sc00 ; vocabulary header
	v_sc00 QWORD noexe ; all searches matching tokens to routines start with this "vocab"
	db offset this word - rootvname ; real-estate of non-reentrant data items for "dir"
	; dq 11D7D34C56431F2Ch ; including xxhash header here causes rootv prefixes to not display
	sc00 BYTE 5, "rootv", 0 
	dq offset sc01
rootv QWORD vocab
	qword offset v_sc00 ; link to first vocabulary item
	byte 7 ; number of slots available here
	byte 6 ; number of slots employed here, must be one less for final '0'

	dword 4794CC60h ; xxhash and fold for rootv, 0 indicates end // 11D7D34C56431F2C
	qword offset rootv ; system calls vocabulary

	dword 8531A604h ; xxhash and fold for "eiv" // 0A65B0B08F5416B4
	qword offset eiv ; execution interpreter vocabulary
 	
	dword 3E98489Ch ; xxhash and fold for "acv" // 63FC55055D641D99
	qword offset acv ; assembly codes vocabulary

	dword 0D205577Ch ; xxhash and fold for "dbg" // 4C0983DA9E0CD4A6
	qword offset dbg ; debug routines vocabulary
	
	dword 7C27416Bh ; xxhash and fold for "bv" // C7FD61E7BBDA208C
	qword offset bv ; build vocabulary

	dword 0DA8F9426h ; xxhash and fold for "alu // ABC3AF77714C3B51
	qword offset alu ; arithmetic and logic vocabulary

	dword 0 ; -- start of 7th set, here 0 indicates end of list
	  endrootv:

byte 7, "restart", 0, msca1 ;    r e s t a r t    sc01 -> sc02
db offset endrestart - this byte, 0
    dq 0C17461139951A54Ch
	sc01 BYTE 7, "restart", 0 ; ***** Initialize Inner Interpreter and run *****
	dq offset sc02 ; sc series ~ a linked list of system codes
restart QWORD restartc ; restart code, the interpreter for the data following in this section
restartic: ; load line buffer with past string, search and execute // initial entry point
	mov rsp, returnstackbottom ; retrieve startup stackptr loc
	; sub rsp, 40h
	mov rbp, parameterstackbottom ; offset pstackspace ; set up parameter stack as offset from register rbp
	mov rbx, offset filereadinterrupt
	mov byte ptr [rbx], 1 ; interrupt any noninteractive mode
	; on entry, rcx points to string sent from main.cpp with \00 indicating the end of the string

	mov rax, offset initialsearchandexe
	push rax
	mov rcx, colonext
	push rcx
	mov rbx, rax ;
	mov rax, [rbx]
	mov rbx, rax
	jmp QWORD ptr [rbx]
initialsearchandexe:
	qword EItoken ;
	qword search ; if found, report executable, if not found return 0
	qword ckFTerror     ; if top of stack 0, report "?" and unfoundtoken and skip remaining threaded interpreter (TI) calls
	qword exei          ; recover previous top of stack from preservetop and go
	qword exitOrReset

;SEH_catch PROC
;	mov rcx, [rsp + 8] ; exception code
;	mov rdx, [rsp + 16] ; exception record pointer
;	ret
;SEH_catch ENDP

restartc: ; entrypoint after exitOrReset -- rebuild EI
	mov rsp, returnstackbottom ; sp_hold ; retrieve startup stackptr loc
	sub rsp, 40h
	mov rbp, parameterstackbottom ; offset pstackspace ; set up parameter stack as offset from register rbp
	mov rbx, offset exitOrResetc
	push rbx ; save recovery option before exit program from "falling out"
	mov rbx, offset filereadinterrupt
	mov byte ptr [rbx], 1 ; interrupt any noninteractive mode
	; on entry, rcx points to string sent from main.cpp with \00 indicating the end of the string
	mov rbx, offset EI
	mov rax, rbx ; code: 48 8B C3 // rax to match EI interpreter colon required jumping to point _colon_
	jmp QWORD ptr [rbx] ; code: FF 23 // at entry, register rbx is a pointer to the data to interpret
	  endrestart:
 
byte 3, "acv", 0, msca1 ; for traceback ;    a c v    __ __ -> ac00    sc02 -> sc03
db offset endacv - this byte, sc02 - this byte
    dq offset acv00 ; stub-link for some searching routines
    v_ac00 QWORD noexe
	dq 63FC55055D641D99h
	acv00 BYTE 3, "acv", 0 ; same name as sc02, but in vocabulary eiv
	qword offset ac00
ac_sub QWORD vocab
	qword offset v_ac00
	byte 0 ; sublinks possible in _ac_ subvocabulary
    dq 63FC55055D641D99h
sc02 BYTE 3, "acv", 0
	dq offset sc03
acv QWORD sysvar
	qword offset v_ac00
	  endacv:

byte 3, "dbg", 0, msca1 ;    d b g   ++ ++ -> db00    sc03 -> sc04
db offset enddbg - this byte, offset sc03 - this byte
dq 4C0983DA9E0CD4A6h
sc03 BYTE 3, "dbg", 0 ;    internal debug vocabulary
	dq sc04 ;
dbg QWORD sysvar
	qword offset v_db00
  BYTE 3, "dbg", 0 ;
    dq offset db00
v_db00 QWORD noexe
;db this word - offset p087a
dq 4C0983DA9E0CD4A6h
db00	BYTE 3, "dbg", 0 ; same name as sc27, but in vocabulary bv
	qword db01
db_sub QWORD sysvar
	qword offset v_db00
	byte 0
	byte 0
      enddbg:

byte 10, "breakpoint", 0, msca1 ;    ++ b r e a k p o i n t ++   db01 -> db02
db offset endbreakpoint - this byte, 0
dq 392A0055CAF8D80Bh
db01 BYTE 10, "breakpoint", 0
	dq db02
breakpoint QWORD machinecode
	pop rbx ; place debug suspend point trap here in tool
	jmp rbx
; place unattached code here to disassemble and place into test.ip via HL
	andn rax,rbx,rdx 
	  endbreakpoint:

byte 8, "chInterp", 0, msca1 ;    __ c h I n t e r p __    ac00 -> ac01
db offset endchInterp - this byte, 0
    dq 0B186DD6FF177F2A0h
	ac00 BYTE 8, "chInterp", 0 ; change interpreter to this one, location on parameter stack [rbp], to the code refrenced at rax
	dq offset ac01 ; ac series ~ assembly codes: linked series for codes employied defining and building token names for execution
	QWORD chInterp
chInterp: 
	mov rcx, parameterstackbottom ; offset pstackspace
	cmp rbp, rcx ; TODO initiate error in that case
	jle abort_chInterp ; abort if parameter stack empty
	sub rbp, 8 ; pop parameter stack, predecrement
	mov rbx, [rbp]
	cmp rbx, 0
	jz abort_chInterp ; abort if parameter stack 0
	add rax, 8 ; advance from token search result to interpreter
	mov [rbx], rax ; cnange interpreter to code refrenced at rax

	pop rbx
	jmp rbx
	abort_chInterp:
	mov rbx, offset filereadinterrupt
	mov byte ptr [rbx], 1 ; interrupt any noninteractive mode
	pop rbx
	jmp rbx
	  endchInterp:

byte 11, "machinecode", 0, msca1 ;    __ m a c h i n e c o d e __    ac01 -> ac02
db offset endmachinecode - this byte, 0
    dq 61B85256152EBC59h
	ac01 BYTE 11, "machinecode", 0 ; indicates data following is self-interpreted assembled native executable
	dq offset ac02 ; TODO consider options for improvement: data parameters marker? include data parameters marker size?
	QWORD chInterp ; interpreter code refrenses routine to change its interpreter to this such
machinecode: add rax, 8
	mov rbx, rax
	jmp rbx
	  endmachinecode:

byte 7, "textout", 0, msca1 ;    __ t e x t o u t __    ac02 -> ac03
db offset endtextout - this byte, offset ac02 - this byte
	extern WriteConsoleA: PROC
	numchars qword ? ; usually unused report of actual numbers of characters actually written from WriteConsoleA
	dq 5A8BEB82DCCC08BAh
	ac02 BYTE 7, "textout", 0
	dq offset ac03
	QWORD chInterp
textout:
	add rax, 8 ; obtain pointer to text to print out
raxtextout: 
	sub rsp, 040h ; shadow stack "out of the way" for WriteConsoleA call
	xor r8, r8
	xor rbx, rbx
	mov bl, [rax]
	mov rcx, ConsoleOutHandle
	add rax, 1
	mov rdx, rax
	mov r8, rbx
	mov r9, offset numchars ; number of characters actually written
	call WriteConsoleA
	add rsp, 040h;
	pop rbx
	jmp rbx
	  endtextout:

byte 8, "ctextout", 0, msca1 ;    __ c t e x t o u t __    ac03 -> ac04
db offset endctextout - this byte, 1
	conditiontextout BYTE 0 ; 1 ~ supress textout once -- 0 ~ textout normally
	dq 6C40CDCAC634EA3Ah
	ac03 BYTE 8, "ctextout", 0
	dq offset ac04
	QWORD chInterp
ctextout:
	mov bl, conditiontextout
	cmp bl, 1
	jz abortctextout
	jmp textout
	abortctextout:
	mov conditiontextout, 0
	pop rbx
	jmp rbx
	  endctextout:

byte 3, "spc", 0, msca1 ;    s p c    sc04 -> sc05
db offset endspc - this byte, 1
	spc equ ' ' ; char
	dq 0A08E289B3420508Fh
	sc04 BYTE 3, "spc", 0
	dq offset sc05
emspc QWORD textout
	byte 1, spc, 0 ; the final ", 0" is not strictly required
	  endspc:

byte 6, "sysvar", 0, msca1 ;    __ s y s v a r __    ac04 -> ac05
db offset endsysvar - this byte, 0
    dq 0E4C554369DCFAFD1h
	ac04 BYTE 6, "sysvar", 0 ; the data following is a system variable offset
	dq offset ac05
	QWORD chInterp
sysvar: add rax, 8
	mov rbx, rax
	mov rax, [rbx]
	mov [rbp], rax
	add rbp, 8
	pop rbx
	jmp rbx
	  endsysvar:

byte 6, "sysact", 0, msca1 ;    __ s y s a c t __    ac05 -> ac06
db offset endsysact - this byte, 0
    dq 2D2BD13DC0D81A39h
	ac05 BYTE 6, "sysact", 0
	dq offset ac06 ; the data following is a system variable at its active position
	QWORD chInterp
sysact: add rbx, 8
	mov [rbp], rbx
	add rbp, 8
	pop rbx
	jmp rbx
	  endsysact:

byte 9, "sysactstr", 0, msca1 ;    __ s y s a c t s t r __    ac06 -> ac07
db offset endsysactstr - this byte, 0
    dq 66BF4E93D3899A06h
	ac06 BYTE 9, "sysactstr", 0	  ; preceded by a byte that describes the length of the actual data
	dq offset ac07 ; \ indicates that the data following is a system variable in its active position
	QWORD chInterp
sysactstr: add rax, 9 ; bypass length byte, giving main address
	mov rbx, rax
	mov [rbp], rbx ;-- only this address is returned, the address after length byte
	add rbp, 8
	pop rbx
	jmp rbx
	  endsysactstr:

byte 5, "noexe", 0, msca1 ;    __ n o e x e __    ac07 -> ac08
db offset endnoexe - this byte, ac06 - this byte
    dq 0D342F33D2648DA65h
    noexeop QWORD noexe ; noexe no operation threaded code example; useful for a "place holder" in a threaded list
	ac07 BYTE 5, "noexe", 0 ; vacuous, null, or noop for "interpreter", default when creating new token marker
	dq offset ac08
	QWORD chInterp
noexe: pop rbx ;
	jmp rbx
	  endnoexe:

byte 4, "noop", 0, msca1 ;    n o o p    sc05 -> sc06
db offset endnoop - this byte, 0
	dq 0F124A67DD286D57Ah
	sc05 BYTE 4, "noop", 0 ; no-op defined this way is useful placing debug suspend
	dq offset sc06
noop QWORD machinecode
	nop ; place debug suspend point trap here in tool
	pop rbx
	jmp rbx
	  endnoop:

byte 8, "colonext", 0, msca1 ;    __ c o l o n e x t __    ac08 -> ac09
db offset endcolonext - this byte, 0
    dq 077A4DFB0C2A2B33h
	ac08 BYTE 8, "colonext", 0 ;
	dq offset ac09 ;                    ***** Inner Interpreter threaded code begin (interpreter for a T-code list) *****
	QWORD chInterp
colonext: pop rax		; retrieve its calling location previously saved
    add rax, 8          ; advance pointer to first or next item on threaded code list
	mov rbx, rax        ; read that first or next item on threaded code list
	; todo compare rax with key interpreter elements
	push rax            ; save its interpreter code
	mov rcx, colonext
	push rcx            ; save to return stack routine to advance to next
	mov rax, [rbx]      ; retrieve its interpreter location
	mov rbx, rax        ; entert: save its interpreter location -- entert now uaually as a manually expanded macro
	jmp QWORD ptr [rbx] ; activate interpreter, where rax is its calling location
	  endcolonext:

byte 5, "colon", 0, msca1 ;    __ c o l o n __    ac09 -> ac10
db offset endcolon - this byte, 0
    dq 0E353F18EFF1F6338h
	ac09 BYTE 5, "colon", 0 ;
	dq offset ac10 ;                    ***** Inner Interpreter threaded code begin (interpreter for a T-code list) *****
	QWORD chInterp
colon: add rax, 8       ; advance pointer to first or next item on threaded code list
	mov rbx, rax        ; macro: include colonext versions ----
	push rax            ; save its interpreter code
	mov rcx, colonext
	push rcx            ; save to return stack routine to advance to next
	mov rax, [rbx]      ; retrieve its interpreter location
	mov rbx, rax        ; entert: save its interpreter location -- entert now uaually as a manually expanded macro
	jmp QWORD ptr [rbx] ; activate interpreter, where rax is its calling location
	  endcolon:

byte 4, "semi", 0, msca1 ;    __ s e m i __    ac10 -> ac11
db offset endsemi - this byte, 0
    dq 0D38E9DAC00998AB2h
	ac10 BYTE 4, "semi", 0 ; final item on threaded code list to signal a return
	dq offset ac11 ;                    ***** Innter Interpreter threaded code end (return from subroutine) *****
semi QWORD OFFSET semic ; Return from threaded subroutine
semic: add rsp, 2*8     ; drop colonext interpreter and its calling position
next: pop rbx			; return from subroutine by activaing its save code position
	jmp rbx
	  endsemi:

byte 5, "vocab", 0, msca1 ;    __ v o c a b __    ac11 -> ac12
db offset endvocab - this byte, 0
    dq 1EE04CEA5B00A1F5h
	ac11 BYTE 5, "vocab", 0 ; vocab is a second linked list of token-words
	dq offset ac12
	QWORD chInterp ; exactly like sysvar, however flags lead vocab for vdir
vocab: add rax, 8
	mov rbx, rax
	mov rax, qword ptr [rbx]
	mov qword ptr [rbp], rax
	add rbp, 8
	pop rbx
	jmp rbx
	  endvocab:

byte 3, "dir", 0, msca1 ;    d i r    sc06 -> sc07
db offset enddir - this byte, offset sc05 - this byte
	holdraxvdir qword ?
	holdorigsrc qword ?
	dq 6EC11D6AFE058D0Eh
	sc06 BYTE 3, "dir", 0 ;
	dq offset sc07
vdir QWORD machinecode
	mov rax, parameterstackbottom ; offset pstackspace ; empty stack?
	cmp rbp, rax
	jnz dirnonemptystackdira ; yes - load rootv by default
	mov rax, offset v_sc00
	mov qword ptr [rbp], rax
	add rbp, 8
	dirnonemptystackdira:
	mov rcx, rbp
	sub rcx, 8 ; get stack element position
	mov rax, qword ptr [rcx] ;
	mov holdorigsrc, rax
	add rax, 9 ; advance to v_sc00, past byte denoting space before label
	xor rdx, rdx
	mov dl, byte ptr [rax]
	add rax, rdx ; advance past name i.e. "rootv"
	add rax, 2 ; advance past lead and trailing size bytes
	add rax, 8 ; advance past downlink

	mov rbx, qword ptr [rax] 
	mov r8, offset vocab
	cmp rbx, r8
	jnz endvdir ; "vocab"? if not, skip this section

	add rax, 16 ; advance past sysvar, offset v_sc00
	mov dh, byte ptr [rax] ; get number of used slots
	cmp dh, 0
	jz endvdir
	add rax, 1
	mov dl, byte ptr [rax] ; get number of filled slots
	add rax, 1

	  subdirloop:
	cmp dl, 0
	jz endvdir
	add rax, 4
	mov qword ptr [rcx], rax
	add rax, 8
	mov holdraxvdir, rax
	mov r14, rdx ; current count
    ;
	mov rbx, offset linecontinue
	push rbx
	mov rax, offset emitsubdir
	mov rbx, rax
	jmp qword ptr [rbx]
      linecontinue:
	mov rdx, r14
	add rbp, 8
	mov rcx, rbp
	sub rcx, 8
	mov rax, holdraxvdir
	sub dl, 1
	jmp subdirloop
	endvdir:
	mov rcx, rbp
	sub rcx, 8
	mov rbx, holdorigsrc
	mov qword ptr [rcx], rbx
	jmp dirc
	;pop rbx
	;jmp rbx
BYTE 10, "emitsubdir", 0
emitsubdir QWORD colon
	qword fetch
	qword xan
	qword emcn
	qword emspc
	qword semi

    dirLbrack BYTE 1, '[', 0
	dirRbrack BYTE 1, ']', 0
	outprechar BYTE 1
	outchar BYTE ?
;BYTE 4, "dirc", 0
;dq 0
;dir QWORD machinecode
dirc: mov rax, parameterstackbottom ; offset pstackspace ; empty stack?
	cmp rbp, rax
	jz diremptystackdira ; yes - load rootv by default
	sub rbp, 8 ; pop from parameter stack predecrement
	mov rdi, qword ptr [rbp]
dirpickentrydir:
	sub rdi, 8 ; retract from interpreter jump code one qword link item offset
	mov rax, qword ptr [rdi] ; advance to next item in chain
	mov rdi, rax

	cmp rdi, 0
	jnz dirloadeddia ; valid entry, continue

	mov rcx, offset direndlist
	push rcx
	mov rax, offset dirLbrack
	jmp raxtextout

diremptystackdira:
	mov rdi, offset v_sc00 
	jmp dirpickentrydir

dirloadeddia:
	mov dl, 255 ; limit loop to 255 iterations
	mov rax, offset dirLbrack
	mov rcx, offset dirnextentry
	push rcx
	mov r14, rdx
	jmp raxtextout

dirnextentry:
	mov rdx, r14
	sub dl, 1
	cmp dl, 0
	jz direndlist

	mov cl, byte ptr [rdi]
	add rdi, 1
	cmp cl, 0
	jnz dirlistcodechar

	add rdi, 1 ; handle null token entry without emiting extra spc
	mov rdi, qword ptr [rdi]
	cmp rdi, 0
	jz direndlist
	jmp dirnextentry

	dirlistcodechar:
	mov al, byte ptr [rdi]
    push rcx
	mov outchar, al
	mov rcx, offset dir_2a
	push rcx
	mov rax, offset outprechar
	mov r14, rdx
	jmp raxtextout

	dir_2a:
	mov rdx, r14
	pop rcx
	add rdi, 1
	sub cl, 1
	jnz dirlistcodechar

	add rdi, 1
	mov rdi, qword ptr [rdi]
	cmp rdi, 0
	jz direndlist

	mov rcx, offset dirnextentry
	push rcx
	mov rbx, offset emspc
	mov rax, rbx
	jmp qword ptr [rbx]

direndlist:
	mov rcx, offset direndlist2
	push rcx
	mov rax, offset dirRbrack
	jmp raxtextout
	direndlist2:
	mov rbx, offset crlf
	mov rax, rbx
	jmp qword ptr [rbx]
	  enddir:

byte 2, "EI", 0, msca1 ;    E I    ***** Outer Interpreter *****    sc07 -> sc08
db offset endEI - this byte, offset sc06 - this byte
	EIstatus BYTE 0 ; nonzero indicates an error, checked with _chXerror_ ; FF indicates a subvocab search on P stack, check with _search_
    dq 0C70CF01A6C8AA4E8h
	sc07 BYTE 2, "EI", 0 ; EI usual outer interpreter
	dq offset sc08 ;
EI QWORD colon ; primary threaded code interpreter
	qword EItoken       ; display prompt and queries the console for next line if required
	qword search        ; if found, report executable, if not found return 0
	qword ckFTerror     ; if top of stack 0, report "?" and unfoundtoken and skip remaining threaded interpreter (TI) calls
	qword exei          ; recover previous top of stack from preservetop and go
	qword ckSUerror     ; if understack, report "!" and last token then skip remaining TI calls
	qword ckXerror      ; if other error reported from exei, report from token and skip remaining TI cals
	qword preservetop   ; hold top of stack for next exei call, enabling routine _top_, which just advances parameter stack pointer
	qword tailcall      ; jump to the next TI element following this call, but at the same return stack level
    qword EI            ; repeat entire EI section, infinite loop, but tailcall keeps this from consuming an additional stack frame
	  endEI:

byte 3, "eiv", 0, msca1 ;    e i v    == == -> eiv00    sc08 -> sc09
db offset endeiv - this byte, offset sc07 - this byte
dq offset eiv00 ; stub-link for some searching routines
  dq 0A65B0B08F5416B4h
  byte 3, "eiv", 0 ; for traceback
  dq offset eiv00
  v_ei00 QWORD noexe
	dq 0A65B0B08F5416B4h
    eiv00 BYTE 3, "eiv", 0
	qword offset ei00
ei_sub QWORD vocab
	qword offset v_ei00
	byte 0 ; sublinks possible in _ei_ subvocabulary
	dq 0A65B0B08F5416B4h
sc08 BYTE 3, "eiv", 0 ;    execution interpreter vocabulary
	dq offset sc09
eiv QWORD sysvar
	qword offset v_ei00
	  endeiv:

byte 13, "ssPstackEmpty", 0, msca1 ;    == s s P s t a c k E m p t y ==    ei00 -> ei01
db offset endssPstackEmpty - this byte, 0
	dq 0F0F660C94BA3D4Fh
	ei00 BYTE 13, "ssPstackEmpty", 0 ; show status parameter stack empty
	dq offset ei01 ; change EIprompt from "EI >" to "EI_>" if parameter stack empty
ssPstackEmpty QWORD machinecode
	mov rax, rbp
	mov rbx, parameterstackbottom ; offset pstackspace
	sub rax, rbx
	jnz notatbottom
	mov al, '_' ; "floor" marker
	jmp breakafternotatbottom
	notatbottom: mov al, spc
	breakafternotatbottom: mov stackbottomnotice, al
	pop rbx 
	jmp rbx
	  endssPstackEmpty:

byte 13, "ssInteractive", 0, msca1 ;        == s s I n t e r a c t i v e ==    ei01 -> ei02
db offset endssInteractive - this byte, 0
    dq 89556993BC897FC5h
	ei01 BYTE 13, "ssInteractive", 0 ; show status interactive session mode
	dq offset ei02 ; change EIprompt from "EI >" to "EI <" if loading noninteractively from file
ssInteractive QWORD machinecode
	mov al, filereadopen
	cmp al, 0
	jz interactive
	mov al, filereadcontinue
	cmp al, 0
	jz nomarkreadnextindicator
		mov al, '[' ; noninteractive form of prompt where additional characters require _filebuf_, however since
		jmp markinteractivity ; \ bufln uaually automatically requests next buffer item, this is not usually seen.
	nomarkreadnextindicator:
	mov al, '<' ; noninteractive form of prompt
	jmp markinteractivity
	interactive:
	mov al, '>' ; interactive form of prompt
	markinteractivity:
	mov noninteractivenotice, al
	pop rbx
	jmp rbx
	  endssInteractive:

byte 8, "EIprompt", 0, msca1 ;    == E I p r o m p t ==    ei02 -> ei03
db offset endEIprompt - this byte, 0
    dq 59FE045F10D56FE2h
	ei02 BYTE 8, "EIprompt", 0 ;
	dq offset ei03 ;
EIprompt QWORD ctextout ; ctextout may be supressed in the case where another prompt is employed
	prompttextlength byte 4 ; permits prompts of up to six characters
	promptpos            byte "EI" ; prompt text position
	stackbottomnotice    byte spc
	noninteractivenotice byte '>' ;
	largestprompt equ 4
	byte largestprompt dup (0) ; provide for future expansion or larget prompt lengths (currently unused)
	  endEIprompt:

byte 9, "promptpos", 0, msca1 ;    == p r o m p t p o s ==    ei03 -> ei04
db offset endpromptpos - this byte, 0
    dq 0A1D410156CFD2E90h
	ei03 BYTE 9, "promptpos", 0 ; prompt text position
	dq offset ei04
	QWORD sysvar
	qword offset promptpos
	  endpromptpos:

byte 8, "infilebe", 0, msca1 ;    == i n f i l e b e ==    ei04 -> ei05
db offset endinfilebe - this byte, 0
    dq 70B2AC97051B20C5h
	ei04 BYTE 8, "infilebe", 0 ; in file buffer examine, first nonexamined char in infliebfr
	dq offset ei05
	QWORD sysact
infilebe byte 0
	  endinfilebe:

byte 8, "infilebf", 0, msca1 ;    == i n f i l e b f ==    ei05 -> ei06
db offset endinfilebf - this byte, 0
	dq 2D8780AF29544E81h
	ei05 BYTE 8, "infilebf", 0
	dq offset ei06
	QWORD sysactstr
infilebfnumchars byte 0
infilebfpastendpos qword ? ; calculated end position of current reading file
	inlnsize equ 127 ; maximum size of input line
	infilesize equ inlnsize
infilebf byte infilesize DUP (?)
	byte 2 DUP (?) ; infilebf overflow space
	  endinfilebf:

byte 6, "inlnbe", 0, msca1 ;    == i n l n b e ==    ei06 -> ei07
db offset endinlnbe - this byte, 0
    dq 0E90095A564815689h
	ei06 BYTE 6, "inlnbe", 0 ; in line buffer examine position
	dq offset ei07
	QWORD sysactstr
maxinln byte inlnsize
inlnbe byte 0
      endinlnbe:

byte 6, "inlnbf", 0, msca1 ;    == i n l n b f ==    ei07 -> ei08
db offset endinlnbf - this byte, 0
    dq 1EF562F33DCFEF54h
	ei07 BYTE 6, "inlnbf", 0 ; Line Read in and processed and interpreted
	dq offset ei08
	QWORD sysactstr
    lnbf_numchars byte ?
	inlnbf byte inlnsize DUP (?)
	byte 8 DUP (?) ; line overflow space
	  endinlnbf:

byte 12, "lnbfPartCopy", 0, msca1 ;    == l n b f P a r t C o p y ==    ei08 -> ei09
db offset endlnbfPartCopy - this byte, 0
	dq 4003CAC2A9C7B364h
	ei08 BYTE 12, "lnbfPartCopy", 0 ; Partial copy of inlnbf
	dq offset ei09 ; first part of inlnbf for routines that modify previous line entered, such as "R"
	QWORD sysact   ; these types of commands are restricted to the first or first _lnbfPartCopy_ characters on input line
	lnbfPartCopyorigsize byte ? ; number of chars in orig buffer
	lnbfPartCopysize equ 2 ; number Quadwords holding lnbfPartCopy, first 8 * lnbfPartCopysize characters
	lnbfPartCopy byte 8 * lnbfPartCopysize DUP (?) ; two Qwords
	  endlnbfPartCopy:

byte 6, "xxhash", 0, msca1 ;    x x h a s h    sc09 -> sc10
db offset endxxhash - this byte, sc09 - this byte
	seed1 dq 0a659100014cb4d67h
	p1 qword  9e3779b185ebca87h
	p2 qword 0c2b2ae3d27d4eb4fh
	p3 qword  165667b19e3779f9h
	p4 qword  85ebca77c2b2ae63h
	p5 qword  27d4eb2f165667c5h
byte 12, "xxhashprompt", 0
dq 0
xxhashprompt QWORD textout
	byte 4, "xx:>", 0
dq 0FDDE0A147CA4F33Ch
sc09 BYTE 6, "xxhash", 0
	dq offset sc10
xxhash QWORD colon
	qword token
	qword xxhashprompt
	qword xxhasht
	qword semi
byte 7, "xxhasht", 0
dq 0
xxhasht QWORD machinecode
	mov r9, seed1;
	add r9, p5 ; h64 = seed + p5

	mov r8, offset inlnbf
	xor rbx,rbx
	mov bl, tokenbegin
	add r8, rbx
	mov bl, tokenlength
	add r9, rbx ; h64 += length

	nextchartokenhash:
	cmp bl, 8
	jl finalDwordtokenstohash
	mov rax, qword ptr [r8]
	mul p2
	rol rax, 31
	mul p1
	xor r9, rax
	add r9, p4
	sub bl, 8
	add r8, 8

	finalDwordtokenstohash:
	cmp bl, 4
	jl finalsinglecharstohash
	xor rax, rax
	mov eax, dword ptr [r8]
	mul p1
	xor r9, rax
	mov r10, r9
	rol r10, 23
	mov rax, p2
	mul r10
	add rax, p3
	mov r9, rax
	sub bl, 4
	add r8, 4
	jmp nextchartokenhash

	finalsinglecharstohash:
	cmp bl, 0
	jz endtokentohash
	xor rax, rax ; mov rax, 0
	mov al, byte ptr [r8]
	mul p5 ; rax, p5
	xor r9, rax
	mov r10, r9
	rol r10, 11
	mov rax, p1
	mul r10
	mov r9, rax

	add r8, 1
	sub bl, 1
	jmp nextchartokenhash ; todo change to finalsinglecharstohash

	endtokentohash:
	mov r10, r9
	shr r10, 33
	xor r9, r10
	mov rax, p2
	mul r9
	mov r9, rax
	mov r10, rax
	shr r10, 29
	xor r9, r10
	mov rax, p3
	mul r9
	shr rax, 32
	xor r9, rax

	mov qword ptr [rbp], r9
	add rbp, 8
	pop rbx
	jmp rbx
	  endxxhash:

byte 4, "fold", 0, msca1 ;    f o l d    sc10 -> sc11
db offset endfold - this byte, 0
    dq 0BA36D6068A66460Eh
sc10 BYTE 4, "fold", 0 ; convert a 64-bit hasn into a 32-bit hash by xor'ing the halves together
	dq offset sc11
fold QWORD machinecode
	mov rcx, rbp
	sub rcx, 8
	mov rax, qword ptr [rcx]
	mov rbx, rax
	shr rbx, 32
	mov rdx, 0FFFFFFFFh
	and rax, rdx
	xor rax, rbx
	mov qword ptr [rcx], rax
	pop rbx
	jmp rbx
	  endfold:

byte 10, "tokensepch", 0, msca1 ;    == t o k e n s e p c h ==    ei09 -> ei10
db offset endtokensepch - this byte, 0
    dq 711A00E99E66E4B7h
	ei09 BYTE 10, "tokensepch", 0 ; token separater character, usually spc or ' '
	dq offset ei10
	QWORD sysact ; no need to define sysactB and sysactQ, as each just pushes address for such
tokensepch byte spc

tokenprefixseparatorcount byte 2
tokenprefixseparator byte '-', '>', 0 ; currently programmed as to expect all characters distinct.

;tokenprefixseparatorcount byte 1 ; system defined with "." as tokenprefixseparater
;tokenprefixseparator byte '.', 0
      endtokensepch:

byte 8, "curtoken", 0, msca1 ;    == c u r t o k e n ==    ei10 -> ei11
db offset endcurtoken - this byte, 0
    dq 39735F4BF0E02A97h
	ei10 BYTE 8, "curtoken", 0
	dq offset ei11
	QWORD sysactstr
tokenbegin byte 0
tokenlength byte 0
nbwr qword ? ; number of bytes actually written to consol, curently unrefrenced except written by PROC call and usually ignored
      endcurtoken:

byte 7, "EItoken", 0, msca1 ;    __ E I t o k e n __    ac12 -> ac13
db offset endEItoken - this byte, 0
    dq 881260451DC26F6Ch
    ac12 BYTE 7, "EItoken", 0 ;
    dq offset ac13
EItoken QWORD machinecode
	EItokencode: mov bh, lnbf_numchars
    mov bl, inlnbe
    cmp bh, bl      ; since whitespace is always appended before actual EOL
    jnz EItokenoEOL ; \ EOL will always be encountered this section
    mov rbx, offset EItokencode ; EOL found, so retrieve new line and continue
    push rbx
    mov rax, offset EIreadLn
    mov rbx, rax ; "entert" MACRO ; execute EIreadln and restart at tokencode
    jmp qword ptr [rbx]
EItokenoEOL:
    mov rsi, offset inlnbf
    xor rbx, rbx
    mov bl, inlnbe
    add rsi, rbx
    mov dl, byte ptr [rsi]
    mov cl, tokensepch
    cmp cl, dl
    jnz EItokeninitialchar
    add bl, 1
    mov inlnbe, bl
    jmp EItokencode ; finding first nonspace might be more efficiently doable TODO
EItokeninitialchar:
    mov rdi, offset tokenprefixseparator
    mov ch, byte ptr [rdi]                ; bl ~ inlnbe
    mov inlnbe, bl ;                      ; bh ~ lnbfnumchars
    mov tokenbegin, bl                    ; cl ~ tokensepch
    mov dh, 0                             ; ch ~ tokenprefixsep
      EItokenintoken:                     ; dl ~ charreading
    add rsi, 1                            ; dh ~ tokenlengthcount
    add bl, 1                             ; rsi ~ ptr to lnbf
    mov dl, byte ptr [rsi]                ; rdi ~ ptr to tokenprefixsep
    cmp dl, cl
    jz EItokendelimatend ; found delimiter whitespace
    cmp dl, ch
    jz EIpartialtokenprefix
    mov rdi, offset tokenprefixseparator ; no match, thus reset tokenprefix
    mov ch, byte ptr [rdi]
    cmp ch, dl
    jz EIpartialtokenprefix
    add dh, 1
    jmp EItokenintoken ; loop back
      EIpartialtokenprefix:
    add rdi, 1
    mov ch, byte ptr [rdi]
    cmp ch, 0
    jz EItokenprefixfound
    add dh, 1
    jmp EItokenintoken
EItokenprefixfound:
	mov rdi, offset eiwrapfrom
	mov byte ptr [rdi], dh
	add rdi, 1
	mov al, byte ptr [tokenbegin]
	mov byte ptr [rdi], al
	add rdi, 1
	mov byte ptr [rdi], bl
	;add bl, 1
	mov inlnbe, bl ; save token length thus far
	;
    mov rdi, offset tokenprefixseparator
    sub rdi, 1
    mov al, byte ptr [rdi] ; al contains the length of the tokenprefixseparator
    mov bh, dh
    sub bh, al
    add bh, 2 ; bh ~ calculate the length of the prefix portion of the token

    mov tokenlength, bh ; BUG off by +1 if already in a vocab subsection
    add bl, 1
    mov inlnbe, bl ; advance token examination past the length of tokenprefixseparator
    mov rax, offset EIwrapsectionfinish
    push rax
    mov rax, offset EIwrapsection ; obtain a 32-bit hash digest of the token prefix
    mov rbx, rax
    jmp qword ptr [rbx]
EIwrapsectionfinish:
    mov rcx, rbp ; parameter stack here contains the 32-bit hash digest of the token prefix
    sub rcx, 8
    mov rax, qword ptr [rcx]        ; rax ~ hash of token prefix to match
    mov rbx, offset rootv
    add rbx, 18 ; skip Qword sysvar, skip Qword v_sc00, skip 2 bytes: no. slots availv, no. slots used

	    EItestwrapentry:
	xor rdx, rdx
	mov edx, dword ptr [rbx]
	cmp rdx, 0 ; rdx is edx expanded -- 0 indicates end of subvocab list
	jz EIrestoresubsection
	cmp rax, rdx
	jz EIwrapmatch
	add rbx, 4 ; past 32bit key
	add rbx, 8 ; past key target
	jmp EItestwrapentry                ; rsi ~ inlnbf
	EIwrapmatch:                       ; bl ~ inlnbe
		add rbx, 4 ; past 32bit key    ; bh ~ lnbufnumchars
		mov rcx, rbp                   ; dl ~ character read in
		sub rcx, 8                     ; dh ~ tokencount
		mov rdx, qword ptr [rbx] ; get target to stack
		add rdx, 8 ; advance past sysvar

		mov rbx, qword ptr [rdx] ; get target of sysvar at vocab entry

        mov qword ptr [rcx], rbx
		mov rbx, offset searchvocabset
		mov al, byte ptr [rbx]
		cmp al, 1
		jnz setEImatch
		nop
		setEImatch:
		mov byte ptr [rbx], 1 ; at this stage we have the search sub in parameter stack and 
		mov cl, tokensepch             ; cl ~ tokensepchar
		mov bl, inlnbe
		mov tokenbegin, bl
        mov rdi, offset tokenprefixseparator
        mov ch, byte ptr [rdi]         ; ch ~ tokenprefixseparator
        mov dh, 0                      ; dh ~ tokencount
		jmp EItokenintoken ; read in token with new search entry namespace in paramater stack place
	EIrestoresubsection:
		mov rdi, offset searchvocabset
		mov byte ptr [rdi], 0 ; undo special token wrap, if set (?)
		mov rdi, offset eiwrapfrom ;
		mov dh, byte ptr [rdi]
		add dh, 1
		add rdi, 1
		mov ah, byte ptr [rdi]
		mov tokenbegin, ah
		add rdi, 1
		mov bl, byte ptr [rdi]
		mov rdi, offset tokensepch
		mov cl, byte ptr [rdi]
		add rdi, 1
		mov al, byte ptr [rdi] ; token separator count
		add bl, al
		mov rdi, offset tokenprefixseparator  ; dh ~ token count
		sub rbp, 8
		jmp EItokenintoken
EIwrapsection QWORD colon
	qword xxhasht
	qword fold
	qword semi
		searchvocabset BYTE 0 ; usual case searchvocab not set
		eiwrapfrom byte ? ; dh
		  byte ? ; token begin
		  byte ? ; inlnbe
		
EItokendelimatend:
	mov inlnbe, bl
	add dh, 1
	mov tokenlength, dh
	mov rbx, offset searchvocabset
	mov ah, byte ptr [rbx]
	cmp ah, 1
	jz skipEIstatusset
        mov bh, 0ffh ; 
        mov EIstatus, bh ; request search employ default rootv
		pop rbx
		jmp rbx
	skipEIstatusset:
	sub dh, 1
	mov tokenlength, dh ; mis-set in this case? why?
	mov byte ptr [rbx], 0 ;
	pop rbx
	jmp rbx
      endEItoken:

byte 5, "token", 0, msca1 ;    __ t o k e n __    ac13 -> ac14
db offset endtoken - this byte, offset ac13 - this byte
	specificprompt qword ? ; hold specific prompt for _token_
	dq 0E670F8FAFEA0A8A0h
	ac13 BYTE 5, "token", 0 ; code for specialty token prompts, token prompt printing call follows this _token_ call in parent if needed, otherwise ignored
	dq offset ac14 ; links to EIreadln for main token separation
token QWORD machinecode
	mov rbx, rsp ; skip following thread interpreter in parent
	add rbx, 8 ; immediate return code remains the same
	mov rcx, qword ptr [rbx] ; address of token prompt one down in stack
	mov rax, rcx
	add rax, 8
	mov qword ptr [rbx], rax ; arrange to skip address of token prompt in RSP stack
	mov specificprompt, rax ; code for prompt
tokennextchar:
	mov bh, lnbf_numchars
	mov bl, inlnbe
	    ;tokennextcharforward:
	cmp bh, bl
	jnz tokennotAtEOL
	mov rbx, offset readatprompt ; EOL found, so retrieve new line with specific prompt
	push rbx
	mov rax, specificprompt
	mov rbx, qword ptr [rax]
	mov rax, rbx
	mov rbx, qword ptr [rax]
	jmp rbx
readatprompt:
	mov conditiontextout, 1 ; supress EI_> prompt since specialty prompt emitted
	mov rbx, offset tokennextchar ; \ _conditiontextout_ resets after EIreadLN
	push rbx ;
	mov rax, offset EIreadLn ; rax required at call
	mov rbx, rax ; "entert" MACRO ; execute EIreadln and restart at tokencode
	jmp qword ptr [rbx]
tokennotAtEOL:
	mov rsi, offset inlnbf
	xor rbx, rbx
	mov bl, inlnbe
	mov cl, tokensepch
	add rsi, rbx
	mov dl, byte ptr [rsi]
	cmp cl, dl
	jnz tokennondelimbegin
	add bl, 1
	mov inlnbe, bl
	jmp tokennextchar
tokennondelimbegin: ; copied from EItokeninitialchar
	mov inlnbe, bl
	mov tokenbegin, bl
	mov ch, 1
		tokenintoken:
	add rsi, 1
	add bl, 1
	mov dl, byte ptr [rsi]
	cmp cl, dl
	jz tokendelimtoend ; found delimiter whitespace
	add ch, 1
	jmp tokenintoken ; loop back
tokendelimtoend:
	mov inlnbe, bl
	mov tokenlength, ch
	pop rbx
	jmp rbx
	  endtoken:

byte 7, "emtoken", 0, msca1 ;    ++ e m t o k e n ++    db02 -> db03
db offset endemtoken - this byte, offset db01 - this byte
	tokenlenrem byte ?
	inlinetokenpos byte ?
	dq 55C24DA8E34B6CB5h
	db02 BYTE 7, "emtoken", 0 ; emit token
	dq db03
emtoken	QWORD machinecode ; 
	sub rsp, 40h ; shadow stack "out of the way" for ConcoleOutHandle subroutine
	xor rcx, rcx
	mov cl, tokenlength
	mov r8, rcx	     ; -- 3 -- in number of characters to be written
	mov cl, tokenbegin
	mov rdx, offset inlnbf
	add rdx, rcx     ; -- 2 -- pointer to a buffer that contains the characters to be written to the console screen
	lea r9, numchars ; -- 4 -- number of characters actually written
	mov rcx, ConsoleOutHandle ; -- 1 --
	push 0           ; reserved: must be null -- 5 --
	call WriteConsoleA
	add rsp, 48h
    pop rbx
    jmp rbx
	  endemtoken:

byte 8, "emline", 0, msca1 ;    ++ e m l i n e ++    db03 -> db04
db offset endemline - this byte, 0
	dq 0B7B90CCCA45801C4h
	db03 BYTE 6, "emline", 0 ; emit line
	dq offset db04
emline QWORD machinecode
	sub rsp, 40h ; move "out of the way" for ConcoleOutHandle subroutine <---
	xor rcx, rcx
	mov cl, lnbf_numchars
	mov r8, rcx	     ; -- 3 -- in number of characters to be written
	mov cl, 0 ; this may be modifiable for a routine like "print remaining line"
	mov rdx, offset inlnbf
	add rdx, rcx     ; -- 2 -- pointer to a buffer that contains the characters to be written to the console screen
	lea r9, numchars ; -- 4 -- number of characters actually written
	mov rcx, ConsoleOutHandle ; -- 1 --
	xor r10, r10     ; -- 5 -- reserved: must be null
	call WriteConsoleA
	add rsp, 40h
    pop rbx
    jmp rbx
	  endemline:

byte 6, "search", 0, msca1 ;    == s e a r c h ==    ei11 -> ei12
db offset endsearch - this byte, offset ei11 - this byte
    nextspecusingvocab qword 0 ; note specific vocab in usinglist search
	dq 0FD461AB8856A01AAh
	ei11 BYTE 6, "search", 0
	dq offset ei12
search dq machinecode
searchc: mov bl, EIstatus ; check EIstatus, if "FF", use rootv and reset EIstatus, otherwise, read vocab entry from parameter stack
	cmp bl, 0FFh
	jnz readvocabentry
	mov bl, 0
	mov EIstatus, bl ; reset EIstatus
	mov rbx, offset usinglist
	add rbx, 8 ; obtain first item in usinglist, to be consulted on failure
	mov nextspecusingvocab, rbx ; setup search 

	mov rax, offset rootv
	add rax, 8 ; advance from qword sysvar to offset v_sc00 link to first vocabulary item
	mov rdi, qword ptr [rax] ; at offset v_sc00
	sub rdi, 8
	mov rax, qword ptr [rdi]
	mov rdi, rax
	jmp searchtokencompare

        readvocabentry:
	mov rbx, 0
	mov nextspecusingvocab, rbx ; clear nextspecusingvocab in the case of a specified root vocab, i.e. "bv->shr"
	sub rbp, 8			; pop stack, predecrement
	mov rdi, qword ptr [rbp]
	sub rdi, 8			; uplink precedes execution address
  	mov rax, qword ptr [rdi]			; obtain next link position
	mov rdi, rax

      searchtokencompare:
	mov rsi, offset inlnbf
	xor rcx, rcx
	mov cl, tokenlength
	cmp cl, byte ptr [rdi]
	jnz searchnomatch		; lengths not matching

	xor rbx, rbx
	mov bl, tokenbegin
	add rsi, rbx			; == si == first char of token
	add rdi, 1			; advance dl to first char in entry
      searchcharcompare:
	cmp byte ptr [rdi], 0		; found sentinel char
	jz searchmatch

	mov rbx, rdi
	cmpsb
	jnz searchnomatch
	jmp searchcharcompare

      searchnomatch:
	mov rdi, rax	; [04] 'e' 'x' 'i' 't' 00 3a 4b
	xor rax, rax
	mov al, byte ptr [rdi]
	add rdi, 2
	add rdi, rax  ;  04 'e' 'x' 'i' 't' 00 [3a] 4b
	mov rax, [rdi]
	cmp rax, 0
	jz serachnotoken

	mov rdi, rax
	mov rax, rdi ; hold current dictionary position

	mov rsi, offset inlnbf
	xor rbx, rbx
	mov bl, tokenlength
	add rsi, rbx

	mov qword ptr [rbp], rdi
	jmp searchtokencompare

      serachnotoken:
	; no match here, set search from using list if such is available
	mov rbx, nextspecusingvocab
	cmp rbx, 0
	jz searchfailure
	mov rax, qword ptr [rbx]
	cmp rax, 0
	jz searchfailure
	mov rcx, rbx
	add rcx, 8
	mov nextspecusingvocab, rcx

	mov rdi, rax
	sub rdi, 8			; uplink precedes execution address
 	mov rax, qword ptr [rdi]			; obtain next link position
	mov rdi, rax
	jmp searchtokencompare

	  searchfailure:
	mov qword ptr [rbp], 0 ; report failure - return 0
	add rbp, 8 ; push via post increment
	pop rbx
	jmp rbx
      searchmatch:
	add rbx, 10 ; one past senteniel, 8 past downlink
	mov qword ptr [rbp], rbx
	add rbp, 8
	pop rbx
	jmp rbx
      endsearch:

byte 9, "ckFTerror", 0, msca1 ;    == c k F T e r r o r ==    ei12 -> ei13
db offset endckFTerror - this byte, offset ei12 - this byte ;    ?? error finding token
dq 591B7AAFCA1611C1h
byte 9, "emFTerror", 0
dq 0
emFTerror QWORD textout
	byte 2, " ?", 0
	ei12 BYTE 9, "ckFTerror", 0 ;
	dq offset ei13 ; find token error
ckFTerror QWORD colon
	qword eierrorc
	qword emFTerror
	qword emtoken
	qword crlf
	qword stackback
	qword EIreadLn
	qword stackforward
	qword aborterrline
	qword ckFTerrorR
byte 10, "ckFTerrorR", 0 ; this is a parent back reference test, sole for tracing
dq 0
ckFTerrorR QWORD parentref
qword ckFTerror
byte 9, "parentref", 0
dq 0
QWORD noexe
parentref dq 0
;
byte 8, "eierrorc", 0
dq 0
eierrorc qword machinecode
	mov rax, parameterstackbottom ; offset pstackspace
	cmp rbp, rax
	jz continuetoerrorline ; continue to error if empty stack
	mov rbx, [rbp-8] ; access top of stack without pop
	xor rax, rax
	cmp rbx, rax
	jz continuetoerrorline ; also continue to error if top of stack would return 0
	add rsp, 2 * 8 ; no error condition, abort from ckFTerror and parent error routine fallout call
	pop rbx
	jmp rbx
	continuetoerrorline:
	mov bl, 1
	mov filereadinterrupt, bl ; if noninteractive, interrupt read next line to isolate error
	pop rbx
	jmp rbx
	;
byte 9, "stackback", 0
dq 0
stackback qword machinecode ; this is to temporarily remove the EI stack point to properly report empty stack status
	sub rbp, 8
	pop rbx
	jmp rbx
byte 12, "stackforward", 0
dq 0
stackforward qword machinecode ; on finding empty stack status, this restores EI stack point for reentry.
	add rbp, 8
	pop rbx
	jmp rbx
byte 12, "aborterrline", 0
dq 0
aborterrline qword machinecode
	nop ; trap point for debug
aborterrlinec: add rsp, 2 * 8 ; abort 
	pop rbx
	jmp rbx
      endckFTerror:

byte 9, "ckSUerror", 0, msca1 ;    == c k S U e r r o r ==    ei13 -> ei14
db offset endckSUerror - this byte, offset ei13 - this byte ;    ?? error stack underflow
dq 0EEBD06D4B2A99182h
byte 9, "emSUerror", 0
dq 0
emSUerror QWORD textout
	byte 2, " !", 0
	ei13 BYTE 9, "ckSUerror", 0 ; report stack underflow (SU) and abort if SU detected
	dq offset ei14
ckSUerror QWORD colon
	qword ckSUerrorc ; check stack, if ok, this thread is returned
	qword emSUerror ; routine continues here for error notification
	qword emtoken
	qword crlf
	qword EIreadLn
	qword aborterrline ; drops first ancestor caller because of error condition
	qword ckSUerrorR

byte 10, "ckSUerrorR", 0 ; parent backlink test
dq 0
ckSUerrorR QWORD parentref
qword ckSUerror

byte 10, "chSUerrorc", 0
dq 0
ckSUerrorc qword machinecode
	mov rax, parameterstackbottom ; offset pstackspace
	cmp rbp, rax
	jl scontinueerrorlinereset
	add rsp, 2 * 8 ; no error condition stack underflow detected, test passed
	pop rbx ;      \ so abandon parent error handling and continue with grandparent caller
	jmp rbx
	scontinueerrorlinereset:  ; error detected, so remainder of parent handles error code
	mov rbp, rax              ; reset stack to floor
	mov bl, 1
	mov filereadinterrupt, bl ; suspend noninteractive mode because stack underflow error was just detected
	pop rbx
	jmp rbx
      endckSUerror:

byte 8, "ckXerror", 0, msca1 ;        == c k X e r r o r ==    ei14 -> ei15
db offset endckXerror - this byte, offset ei14 - this byte ;    ?? reported extra error
dq 0E59003964C83731h
byte 8, "emXerror", 0
dq 0
emXerror QWORD textout
	byte 3, " ><", 0
ei14 BYTE 8, "ckXerror", 0 ;
	dq offset ei15 ; find token error
ckXerror QWORD colon ;
	qword ckXerrorc ; check stack, if ok, this thread is returned
	qword emXerror ; routine continues here for error notification
	qword emtoken
	qword crlf
	qword EIreadLn
	qword aborterrline ; drops first ancestor caller because of error condition
byte 9, "ckXerrorc", 0
dq 0
ckXerrorc qword machinecode
	mov al, EIstatus
	cmp al, 0
	jnz Xcontinueerrorlinereset
	add rsp, 2 * 8 ; no error condition detected, test passed
	pop rbx ;      \ so abandon parent error handling and continue with grandparent caller
	jmp rbx
	Xcontinueerrorlinereset:  ; error detected, so remainder of parent handles error code
	mov bl, 1
	mov filereadinterrupt, bl ; suspend noninteractive mode error was just reported
	mov bh, 0
	mov EIstatus, bh ; reset EIstatus, having handled error
	pop rbx
	jmp rbx
      endckXerror:

byte 4, "exei", 0, msca1 ;    == e x e i ==    ei15 -> ei16
db offset endexei - this byte, 0
	dq 4591515BAE1DA265h
	ei15 BYTE 4, "exei", 0
	dq offset ei16
exei QWORD machinecode
	mov rax, parameterstackbottom ; offset pstackspace
	sub rbp, 8 ; pop stack by pre decrement
	mov rbx, [rbp]
	xor rax, rax
	cmp rbx, rax
	jnz exego			; also abort if retrieved 0
	pop rbx
	jmp rbx
	  exego:	
	mov rax, tophold		; retrieve previous "overtop" element on stack
	mov [rbp], rax
	mov rax, rbx			; ax points to location of interpreter
	jmp qword ptr [rbx]		   		; jump to interpreter of threaded element
      endexei:

byte 7, "tophold", 0, msca1 ;    == t o p h o l d ==    ei16 -> ei17
db offset endtophold - this byte, 0
    dq 4591515BAE1DA265h
	ei16 BYTE 7, "tophold", 0
	dq offset ei17
	QWORD sysact
tophold	qword ?
      endtophold:

byte 3, "top", 0, msca1 ;    == t o p ==    ei17 -> ei18
db offset endtop - this byte, 0
    dq 9CBFA767A758CB50h
    ei17 BYTE 3, "top", 0
	dq ei18
    top QWORD machinecode
	add rbp, 8 ; retrieve previous topmost item on stack
	pop rbx
	jmp rbx
      endtop:

byte 11, "preservetop", 0, msca1 ;    == p r e s e r v e t o p ==    ei18 -> 0
db offset endpreservetop - this byte, 0
    dq 0DA230538329B12B5h
	ei18 BYTE 11, "preservetop", 0
	dq 0 ; no following link in ei series
preservetop QWORD machinecode
	mov rbx, [rbp]
	mov tophold, rbx	; preserve overtop element on stack
	pop rbx				; _exei_ retrieves this to stack
	jmp rbx				; before executing
      endpreservetop:

byte 8, "EIreadLn", 0, msca1 ;    ++ E I r e a d L n ++    db04 -> db05
db offset endEIreadLn - this byte, 0
    dq 0AC94784FBCC7372Eh
	db04 BYTE 8, "EIreadLn", 0 ; comment to EOL
	dq offset db05
EIreadLn QWORD colon
	qword ssPstackEmpty
	qword ssInteractive
	qword noninteractivebranch3 ; if detecting noninteractive, skip ahead to line comment "A"
    qword EIprompt
	qword consoleReadln
	qword semi ; end standard interactive line

	qword ifemptybuf_filebuf    ; line comment "A": from detecting noninteractive, retrieve line from filebuf
	qword ifclosed_abort
	qword EIprompt
	qword bufln
	qword semi
BYTE 21, "noninteractivebranch3", 0 ; identify routine for tracebacks
dq 0
noninteractivebranch3 QWORD machinecode
	mov bl, filereadopen
	cmp bl, 0 ;
	jz notskipparentforward2
	mov bh, filereadinterrupt ; error conditions will halt autoreading with this flag
	cmp bh, 1 ; interrupt autocomplete
	jz notskipparentforward2
		mov rbx, rsp
		add rbx, 8
		mov rax, qword ptr [rbx]
		add rax, 3 * 8
		mov qword ptr [rbx], rax
	notskipparentforward2:
	pop rbx
	jmp rbx
      endEIreadLn:

byte 2, "//", 0, msca1 ;    / /    sc11 -> sc12
db offset endslashslash - this byte, 0
	dq 0C22F3D07D64197E1h
sc11 byte 2, "//", 0
	dq sc12
	QWORD alias_
	qword EIreadLn
      endslashslash:

byte 5, "emrsp", 0, msca1 ;    ++ e m r s p ++    db05 -> db06
db offset endemrsp - this byte, 0
	dq 471203FAE6C205D8h
	db05 BYTE 5, "emrsp", 0 ; emit rsp
	dq offset db06 ; this is useful for checking for any stack leaks and verifying routines exit cleanly
emrsp QWORD machinecode
	mov rbx, rsp
	jmp displayrbx ; transfer to patch in hexo
      endemrsp:

byte 5, "emrbp", 0, msca1 ;    ++ e m r b p ++    db06 -> db07
db offset endemrbp - this byte, 0
	dq 0A81927E9F9C3DA04h
	db06 BYTE 5, "emrbp", 0 ; emit rbp, similar for testing any paramter stack leaks
	dq offset db07
emrbp QWORD machinecode
	mov rbx, rbp
	jmp displayrbx ; transfer to patch in hexo
      endemrbp:

byte 4, "crlf", 0, msca1 ;    c r l f    sc12 -> sc13
db offset endcrlf - this byte, 0
    dq 519972BA0345CE1Ch
	sc12 BYTE 4, "crlf", 0
	dq offset sc13
crlf QWORD textout
	byte 2, 0Dh, 0Ah
      endcrlf:

byte 8, "tailcall", 0, msca1 ;    __ t a i l c a l l __    ac14 -> ac15
db offset endtailcall - this byte, 0
	dq 0A27E3214D0937409h
	ac14 BYTE 8, "tailcall", 0 ;
	dq offset ac15
tailcall QWORD machinecode ; tailcall is a frameless or nonrecursive tailcall, to avoid increasing stack
	mov rax, rsp 
	add rax, 8 ; observe position address in threaded list of this tailcall routine
	mov rcx, rax ; retain in rcx position in stack to be replaced with tailcall
	mov rbx, qword ptr [rax] ; get address of tailcall in parent referencing this routine
	add rbx, 8 ; advance to next item in threaded call of parent, to be threaded into this level
	mov rax, qword ptr [rbx] ; interpreter (should be colon) or one item before item to tailcall in this same stack level
	mov qword ptr [rcx], rax ; change uncle call to nephew, same call level
	pop rbx ; activate colonext TODO expand macro
	jmp rbx
      endtailcall:

byte 1, "Q", 0, msca1 ;    Q    sc13 -> sc14    o   
db offset endQ - this byte, 0
	dq 0A51D4FF74D234CCAh
	sc13 BYTE 1, 'Q', 0 ; when activated, drops the current interpreter
	dq offset sc14      ; the lowest interpreter shell drops out to exitOrReset
	qword offset semic
      endQ:

byte 4, "hexi", 0, msca1 ;    h e x i    sc14 -> sc15
db offset endhexi - this byte, offset sc10 - this byte
	BYTE 9, "promptnum", 0 ; back trace name
	qword 0
promptnum QWORD textout
	BYTE 3, "# >", 0
    tokenbfposloc qword 0
    ihexibfloc qword 0
    significantfigurescount BYTE 0
    tokenlenremain BYTE 0
    badnumerrmsg BYTE 2, "# "
	BYTE 6, "hexi >", 0
	dq 2A2A064B939ECFEAh
sc14 BYTE 4, "hexi", 0
	dq offset sc15
hexi QWORD colon
	qword token
	qword promptnum
	qword hexii
	qword semi
	;qword hexiR
;
;byte 5, "hexiR", 0 ; this is a parent back test
;dq 0
; hexiR QWORD parentref
;qword hexi
;
BYTE 5, "hexii", 0 ; trace naming code
qword 0
hexii QWORD machinecode
hexicc: xor rcx, rcx
	mov significantfigurescount, ch ; no signficant figures yet
	mov ihexibfloc, rcx		; clear out ihexifbloc
	mov rbx, OFFSET inlnbf
	mov al, tokenlength
	mov tokenlenremain, al
	mov cl, tokenbegin		; ch remains 0
	add rbx, rcx
      hexichar:
        mov tokenbfposloc, rbx
	mov al, [rbx]
        cmp al, '0'
        jl hexi09
        cmp al, '9'
        jg hexi09
        sub al, '0'
        jmp hexiaccum
      hexi09:
        cmp al, 'A'
        jl hexiAF
        cmp al, 'F'
        jg hexiAF
        sub al, 'A'-10
        jmp hexiaccum
      hexiAF:
        cmp al, 'a'
        jl hexiabort
        cmp al, 'f'
        jg hexiabort
        sub al, 'a'-10
      hexiaccum: ;good entry
        mov rbx, ihexibfloc
	shl rbx, 4
	xor ah, ah
        add bx, ax

        mov ihexibfloc, rbx
	mov cl, tokenlenremain
	sub cl, 1
	cmp cl, 0
	jz hexitoken

	mov tokenlenremain, cl
	cmp rbx, 0
	jz nosignificantfigure

	mov al, significantfigurescount
	add al, 1
	mov significantfigurescount, al
	cmp al, 16
	jge hexiabort ; number register overflow

	nosignificantfigure: mov rbx, tokenbfposloc
	add rbx, 1
        jmp hexichar
      hexiabort: ;bad entry
	;mov rdx, OFFSET badnumerrmsg
	mov al, 1
	mov EIstatus, al
	pop rbx
	jmp rbx
  hexitoken: ;completed
        mov rbx, ihexibfloc
        mov qword ptr [rbp], rbx
        add rbp, 8
        pop rbx
        jmp rbx
      endhexi:

byte 4, "hexB", 0, msca1 ;    h e x B    sc15 -> sc16
db offset endhexB - this byte, offset sc11 - this byte
	hexbmsg BYTE "GG", 0
	dq 0FDEA4C1C6F155543h
	sc15 BYTE 4, "hexB", 0 ; output single byte at address
	dq offset sc16
hexb QWORD machinecode
hexboc:
	sub rbp, 8 ; pop parameter stack via predecrement
	mov rbx, [rbp]
	mov al, bl
	mov rdi, offset hexbmsg
hexbout:	
	mov ah, al ; save byte to be displayed
	mov cl, 4  
	shr al, cl ; get high 4 bits
	cmp al, 9
	jle numb1h ; in range 0..9
	add al, "A" - 10
	jmp countb1h
numb1h:	add al, "0"
countb1h: mov byte ptr [rdi], al
	add rdi, 1
	mov al, ah ; recover byte to be displayed
	and al, 0Fh ; mask out high 4 bits
	cmp al, 9
	jle numb2h
	add al, "A" - 10
	jmp countb2h
numb2h:	add al, "0"
countb2h: mov byte ptr [rdi], al
	add rdi, 1
hexobe:
	sub rsp, 40h ; move "out of the way" for ConcoleOutHandle subroutine <---
	xor rcx, rcx
	mov cl, 2
	mov r8, rcx	               ; -- 3 -- in number of characters to be written
	mov rdx, offset hexbmsg ;  ; -- 2 -- pointer to a buffer that contains the characters to be written to the console screen
	lea r9, numchars           ; -- 4 -- number of characters actually written
	mov rcx, ConsoleOutHandle  ; -- 1 --
	xor r10, r10               ; -- 5 -- reserved: must be null
	call WriteConsoleA
    ;
	add rsp, 40h
	pop rbx
	jmp rbx	
      endhexB:

byte 4, "hexo", 0, msca1 ;    h e x o    sc16 -> sc17
db offset endhexo - this byte, offset sc12 - this byte
	hexqmsg BYTE "0xGGHHGGHHGGHHGGHH", 0
	hexqbytecount BYTE 8 ;
	dq 0CB968FF2155DE75Ah
	sc16 BYTE 4, "hexo", 0 ; output single byte at address
	dq offset sc17
hexq QWORD machinecode
hexoutc:
	sub rbp, 8 ; pop parameter stack via predecrement
	mov rbx, [rbp]
displayrbx: mov al, bl ; useful as tailcall jump for diagnostic routines
	mov hexqbytecount, 8 ; reset to 8 bytes to display
	mov rdi, offset hexqmsg
	add rdi, 16 ;
hexout:	
	mov ah, al ; save byte to be displayed
	mov cl, 4  
	shr al, cl ; get high 4 bits
	cmp al, 9
	jle num1h ; in range 0..9
	add al, "A" - 10
	jmp count1h
num1h:	add al, "0"
count1h: mov byte ptr [rdi], al
	add rdi, 1
	mov al, ah ; recover byte to be displayed
	and al, 0Fh ; mask out high 4 bits
	cmp al, 9
	jle num2h
	add al, "A" - 10
	jmp count2h
num2h:	add al, "0"
count2h: mov byte ptr [rdi], al
	sub rdi, 3
	;xor rcx, rcx
	mov cl, hexqbytecount
	sub cl, 1
	mov hexqbytecount, cl
	cmp cl, 0
	jz hexoe
	mov cl, 8
	shr rbx, cl
	mov rax, rbx
	jmp hexout
hexoe:
	sub rsp, 40h ; move "out of the way" for ConcoleOutHandle subroutine <---
	xor rcx, rcx
	mov cl, 18
	mov r8, rcx	               ; -- 3 -- in number of characters to be written
	mov rdx, offset hexqmsg ;  ; -- 2 -- pointer to a buffer that contains the characters to be written to the console screen
	lea r9, numchars           ; -- 4 -- number of characters actually written
	mov rcx, ConsoleOutHandle  ; -- 1 --
	xor r10, r10               ; -- 5 -- reserved: must be null
	call WriteConsoleA

	add rsp, 40h
	pop rbx
	jmp rbx	
      endhexo:

byte 5, "hexBo", 0, msca1 ;    h e x B o    sc17 -> sc18    TODO
db offset endhexBo - this byte, offset sc17 - this byte
	hexBqmsg BYTE "0xGG", 0
	;hexqbytecount BYTE 8 ;
	sc17 BYTE 5, "hexBo", 0 ; output single byte at address
	dq offset sc18
hexBq QWORD machinecode
;hexoutc:
;	sub rbp, 8 ; pop parameter stack via predecrement
;	mov rbx, [rbp]
;displayrbx: mov al, bl ; useful as tailcall jump for diagnostic routines
;	mov hexqbytecount, 8 ; reset to 8 bytes to display
;	mov rdi, offset hexqmsg
;	add rdi, 16 ;
;hexout:	
;	mov ah, al ; save byte to be displayed
;	mov cl, 4  
;	shr al, cl ; get high 4 bits
;	cmp al, 9
;	jle num1h ; in range 0..9
;	add al, "A" - 10
;	jmp count1h
;num1h:	add al, "0"
;count1h: mov byte ptr [rdi], al
;	add rdi, 1
;	mov al, ah ; recover byte to be displayed
;	and al, 0Fh ; mask out high 4 bits
;	cmp al, 9
;	jle num2h
;	add al, "A" - 10
;	jmp count2h
;num2h:	add al, "0"
;count2h: mov byte ptr [rdi], al
;	sub rdi, 3
	;xor rcx, rcx
;	mov cl, hexqbytecount
;	sub cl, 1
;	mov hexqbytecount, cl
;	cmp cl, 0
;	jz hexoe
;	mov cl, 8
;	shr rbx, cl
;	mov rax, rbx
;	jmp hexout
;hexoe:
;	sub rsp, 40h ; move "out of the way" for ConcoleOutHandle subroutine <---
;	xor rcx, rcx
;	mov cl, 18
;	mov r8, rcx	               ; -- 3 -- in number of characters to be written
;	mov rdx, offset hexqmsg ;  ; -- 2 -- pointer to a buffer that contains the characters to be written to the console screen
;	lea r9, numchars           ; -- 4 -- number of characters actually written
;	mov rcx, ConsoleOutHandle  ; -- 1 --
;	xor r10, r10               ; -- 5 -- reserved: must be null
;	call WriteConsoleA

;	add rsp, 40h
	pop rbx
	jmp rbx	
      endhexBo:

byte 5, "fetch", 0, msca1 ;    f e t c h    sc18 -> sc19
db offset endfetch - this byte, 0
	dq 3EA4C8D831E57F96h
	sc18 BYTE 5, "fetch", 0 ; retrieve memory contents at address on parameter stack
	dq offset sc19
fetch QWORD machinecode
	mov rbx, rbp
	sub rbx, 8 ; hold predecrement Pstack place for data and return
	mov rax, qword ptr [rbx] ; get address off stack top
	mov rcx, rax
 	mov rax, qword ptr [rcx] ; get destination
	mov [rbx], rax ; place fetch to stack top hold postincrement position
 
;ExceptionHandler PROC ExceptionInfo:QWORD
;	mov eax, EXCEPTION_CONTINUE_EXECUTION
;	ret
;ExceptionHandler endp

;invoke AddVectoredExceptionHandler, 1, offset ExceptionHandler

	pop rbx
	jmp rbx
	  endfetch:

byte 6, "fetchB", 0, msca1 ;    f e t c h B    sc19 -> sc20
db offset endfetchB - this byte, 0
    dq 0A57C2466E924ABD2h
	sc19 BYTE 6, "fetchB", 0
	dq offset sc20
fetchB QWORD machinecode
	mov rbx, rbp
	sub rbx, 8 ; hold predecrement Pstack place for data and return
	mov rax, qword ptr [rbx] ; get address off stack top
	mov rcx, rax
	xor rax, rax
 	mov al, byte ptr [rcx] ; get destination
	mov [rbx], rax ; place fetch to stack top hold postincrement position
	pop rbx
	jmp rbx
      endfetchB:

byte 4, "dump", 0, msca1 ;    d u m p    sc20 -> sc21
db offset enddump - this byte, offset sc20 - this byte
	locat qword ?
	holdrbx qword ?
	remln byte ?
	dumplinesrem byte ?
	dumpmsg byte 3 
	  dumpmsgloc byte "FF", spc
	linedash byte 2, '-', spc, 0
	dumpcharmsg byte 1 
	  dumpcharloc byte ?
	zerodumpmsg byte 5, "NULL ", 0
    dq 8829D0C17808EA80h
	sc20 BYTE 4, "dump", 0 ;
	dq offset sc21
dump QWORD machinecode
	mov rax, parameterstackbottom ; offset pstackspace ; empty stack?
	cmp rbp, rax
	jnz dumpnotemptystack ; yes - retrieve top value by default
	add rbp, 8
dumpnotemptystack:
	mov rax, rbp
	sub rax, 8
	mov rbx, qword ptr [rax]
	cmp rbx, 0
	jz zerodump
	mov locat, rbx
	mov al, 8
	mov dumplinesrem, al

dumplinesloop:
	mov al, 16
	mov remln, al
	mov rax, dumplinehead1
	push rax
	jmp hexoutc

dumplinehead1:
	mov rcx, dumplinehead2
	push rcx
	mov holdrbx, rbx
	mov rbx, offset emspc
	mov rax, rbx
	jmp qword ptr [rbx]
dumplinehead2:
	mov rbx, locat
dumploop:
	mov rdi, offset dumpmsgloc
	mov al, byte ptr [rbx]
	add rbx, 1
hexdout:
	mov ah, al
	mov cl, 4
	shr al, cl
	cmp al, 9
	jle numd1h
	add al, "a"-10
	jmp countd1h
numd1h: add al, "0"
countd1h: mov byte ptr [rdi], al
	add rdi, 1
	mov al, ah
	and al, 0fh
	cmp al, 9
	jle numd2h
	add al, "a" - 10
	jmp countd2h
numd2h: add al, "0"
countd2h: mov byte ptr [rdi], al
dumpout:
	mov rcx, offset dumpendlist
	mov holdrbx, rbx
	push rcx
	mov rax, offset dumpmsg
	jmp raxtextout
dumpendlist:
	mov rbx, holdrbx
	mov al, remln
	sub al, 1
	mov remln, al
	cmp al, 0
	jz dumpAF
	cmp al, 8
	jz inlinedash
	jmp dumploop
inlinedash:
	mov holdrbx, rbx
	mov rax, offset dumploopresume
	push rax
	mov rax, offset linedash
	jmp raxtextout
	dumploopresume:	mov rbx, holdrbx
	jmp dumploop
dumpAF:
	mov rbx, locat
	mov al, 16
	mov remln, al

		dumpcharloop:
	mov rbx, locat
	mov cl, byte ptr [rbx]
	add rbx, 1
	mov locat, rbx
	cmp cl, spc
	jl ctrlchar
	cmp cl, '~'
	jg ctrlchar
	jmp dumpcharout
	ctrlchar: mov cl, "."
		dumpcharout:
	mov dumpcharloc, cl
	mov rdx, offset dumpcharmsg

	mov rax, offset dumpAF2
	push rax
	mov rax, offset dumpcharmsg
	jmp raxtextout
	dumpAF2: mov al, remln
	sub al, 1
	mov remln, al
	cmp al, 0
	jz dumpcharexit
	jmp dumpcharloop

dumpcharexit:
	mov rax, dumplinesloopset
	push rax
	mov rbx, offset crlf
	mov rax, rbx
	jmp qword ptr [rbx]

	dumplinesloopset:
	mov al, dumplinesrem
	sub al, 1
	mov dumplinesrem, al
	cmp al, 0
	jz dumproutinexit

	mov rbx, locat
	mov [rbp], rbx
	add rbp, 8
	jmp dumplinesloop

zerodump:
	mov rax, offset zerodumpmsg
	sub rbp, 8 ; abort stack
	jmp raxtextout

dumproutinexit:
	mov rbx, [rbp]
	add rbx, 10h
	mov [rbp], rbx
	pop rbx
	jmp rbx
      enddump:

byte 10, "entrypoint", 0, msca1 ;    e n t r y p o i n t    sc21 -> sc22
db offset endentrypoint - this byte, 0
    dq 2CB1AF15CB7CF593h
	sc21 BYTE 10, "entrypoint", 0 ;
	dq offset sc22
	QWORD sysvar
	QWORD offset entrypoint
      endentrypoint:

byte 8, "openfile", 0, msca1 ;    o p e n f i l e    sc22 -> sc23
db offset endopenfile - this byte, sc16 - this byte
	  byte 15, "filenotfoundmsg", 0
	qword 0
	filenotfoundmsg qword textout
	byte 16, "file not found: ", 0
      byte 14, "filenameprompt", 0
	qword 0
	filenameprompt qword textout
	byte 24, "enter filename to open: ", 0
	dq 0B2D4C3B30E5C2C5Ah
	sc22 BYTE 8, "openfile", 0 ; interpret file: open handle to 
	dq offset sc23
openfile QWORD colon
	qword token
	qword filenameprompt
	qword processtokenfilename ; returns if filename open
	qword emtoken ; this is error routine if filename not found
	qword crlf
	qword EIreadLn
	qword aborterrline
	qword semi
	extern CreateFileA: PROC
	extern GetLastError: PROC
    byte 20, "processtokenfilename", 0
	qword 0
processtokenfilename qword machinecode
processtokenfilenamec:
	add rbp, 8
	mov qword ptr [rbp], rsp ; employment of r15 replaced with parameter stack
	sub rsp, 40h ;
	mov rsi, offset inlnbf ; peel file name out of inline, and convert to a null terminated filename string
	xor rbx, rbx
	mov bl, tokenbegin
	add rsi, rbx ; points to first char in linebuffer "file name"
	mov cl, tokenlength
	mov rdi, offset filereadname
infilenamenextchar: cmp cl, 0
	jz infilenamecomplete
	movsb
	sub cl, 1
	jmp infilenamenextchar
	infilenamecomplete:
	mov [rdi], cl ; terminate filename line with byte 00
		generic_read equ 080000000h
		file_share_read equ 01h
		file_attribute_normal equ 080h
		open_existing equ 3
	mov rcx, offset filereadname       ; in LPCSTR lpFileName
    mov rdx, generic_read              ; qword DesiredAccess
	mov r8, file_share_read		       ; lpSecurityAttributes
	xor r9, r9 ; lpSeciruty attributes ; null dwCreationDisposition
	push 0 ; creadtionDisposition      ; dwCreationDisposition
    push file_attribute_normal         ; dwFlagsAndAtributes
	push open_existing                 ; hTemplateFile
	push 0 ;
	sub rsp, 24 ; shadow stack for four registers
		call CreateFileA
	cmp rax, -1
	jz infileerror
	mov filereadhandle, rax
	mov bl, 1
	mov filereadopen, bl ; mark file open
	mov byte ptr filereadcontinue, 1 ; note that infile buffer infilebe empty at opening file
	mov byte ptr infilebfnumchars, 0 ; zero out infile buffer on opening file
	mov byte ptr filereadinterrupt, 1 ; openfile directly does not continue to read automatically
	mov infilebe, 0 ; reset file read buffer
	mov rsp, qword ptr [rbp]
	sub rbp, 8
	add rsp, 16 ; drop from back away error trap; routine successful

	mov rax, offset infilebf ; zero out infilebf on opening a new filename
	mov infilebfpastendpos, rax
	mov byte ptr infilebe, 0
	mov byte ptr infilebfnumchars, 0

	pop rbx
	jmp rbx
	;
	infileerror:
	call GetLastError
	cmp ax, 2
	jz infilenofound
	nop ; other error
	infilenofound:
	mov rsp, qword ptr [rbp]
	sub rbp, 8
	mov rax, offset filenotfoundmsg
	mov rbx, rax
	jmp qword ptr [rbx]
      endopenfile:

byte 10, "filestatus", 0, msca1 ;    __ f i l e s t a t u s __    ac15 -> ac16
db offset endfilestatus - this byte, 0
	dq 0A31A1653B5D670B4h
	ac15 BYTE 10, "filestatus", 0 ; file handle indicators
	dq ac16
	QWORD sysact
filereadcontinue byte 0 ; 0 indicates complete copy, 1 indicates need additional bytes
filereadopen byte 0 ; 0 indicatges console read, other indicates file read
filereadinterrupt byte 0 ; 0 indicates no interrupt, other indicates pause automatic read next
byte '>' ; marker: curious if "align 8" inserts any 0's or nop's
align 8
filereadhandle qword ?
byte '<'
filereadname qword 8 dup (?) ; TODO possibly include a quadword "digest" to avoid reopening, except for path and abbreviated path?
      endfilestatus:

byte 8, "autoread", 0, msca1 ;    a u t o r e a d    sc23 -> sc24
db offset endautoread - this byte, 0
    dq 64DA1E0115739A41h
	sc23 BYTE 8, "autoread", 0 ;
	dq sc24
autoread QWORD machinecode
	mov bl, 0
	mov filereadinterrupt, bl ; clear filereadinterrupt flag, set on any error in noninteractive mode
	pop rbx
	jmp rbx
      endautoread:

byte 9, "closefile", 0, msca1 ;    c l o s e f i l e    sc24 -> sc25
db offset endclosefile - this byte, offset sc24 - this byte
	extern CloseHandle: PROC
	dq 6B29C444338F137Fh
	sc24 BYTE 9, "closefile", 0
	dq sc25
closefile QWORD machinecode
	mov bl, filereadopen
	cmp bl, 0
	jz abortclosefile
	mov bl, 0
	mov filereadopen, 0 ; set openfile flag false
	mov rcx, filereadhandle
		call CloseHandle
	abortclosefile:
	pop rbx
	jmp rbx
	  endclosefile:

byte 13, "consoleReadln", 0, msca1 ;    c o n s o l e R e a d l n    sc25 -> sc26
db offset endconsoleReadln - this byte,  offset sc19 - this byte
	extern ReadConsoleA: PROC
	dq 6B29C444338F137Fh
	sc25 BYTE 13, "consoleReadln", 0 ; read input line from console input
	dq offset sc26
	consolereadln QWORD machinecode ; TODO place here LoadFile link: each defined word should link to its loadfile definition
	xor rax,rax
	mov al, inlnbe
    ;
	cmp al, 0
	jz readlnskplnbfPartCopy ; skip if empty line, such as with startup
	cmp al, 1
	jnz copylinepart
	mov rsi, offset inlnbf
	xor rcx, rcx
	mov cl, [rsi]
	mov dl, tokensepch
	cmp cl, dl
	jz readlnskplnbfPartCopy ; skip if empty line with only spc ' ' character, seen on empty line
	;
	copylinepart: ; preserve the first sixteen characters (2 QWORDS) of line for line examination routines, such as R
	mov al, lnbf_numchars
	mov lnbfPartCopyorigsize, al
	mov rdi, offset lnbfPartCopy ; copy the first 
	mov rsi, offset inlnbf
	mov rcx, [rsi]
	mov [rdi], rcx
	add rdi, 8
	add rsi, 8
	mov rcx, [rsi]
	mov [rdi], rcx
	;
	readlnskplnbfPartCopy:
 	mov RCX, ConsoleInHandle     ; in HANDLE hConsoleInput -- handle to the console input buffer
	lea RDX, inlnbf ; out LPVOID lpBuffer -- buffer that receives the data read from the console input buffer
	mov r8, inlnsize      ; in nNumberOfCharsToRead -- The number of characters to be read. 
	;					  ; The size of the buffer pointed to by the lpBuffer parameter should be at least nNumberOfCharsToRead * sizeof(TCHAR) bytes.
	lea r9, nbwr		  ; out LPDWORD lpNumberOfCharsRead -- A pointer to a variable that receives the number of characters actually read.
	xor r10,r10			  ; inout LPVOID pInputControl, set to NULL -- A pointer to a CONSOLE_READCONSOLE_CONTROL structure that specifies a control character
	;					  ; to signal the end of the read operation.  This parameter can be NULL
	call ReadConsoleA
	mov rdx, offset inlnbf
	mov rbx, nbwr
	sub rbx, 2 ; 	      ; do not count CR LF or ^M ^J or 0x0D 0x0A at end of input line
	add rdx, rbx
	mov al, tokensepch
	mov byte ptr [rdx], al ; replace 0x0D or ^M or Carriage Return Code with space
	add bl, 1				; this guarantees that EOL need only be checked where the char is whitespace
	mov lnbf_numchars, bl ; report number of characters in line, including final space but not final CR LF
	mov rdx, offset inlnbe ; 
	mov byte ptr [rdx], 0 ; zero out examined position in current line, since a new line is read in
	add rsp, 2 * 8		  ; FIX macro, semi drop current machinecode call, otherwise recalls consolereadln
	pop rbx
	jmp rbx
      endconsoleReadln:

byte 7, "filebuf", 0, msca1 ;    f i l e b u f    sc26 -> sc27
db offset endfilebuf - this byte, offset sc26 - this byte
	extern ReadFile: PROC
	dq 0AAF7C746747E9E96h
	sc26 BYTE 7, "filebuf", 0 ; fill buffer from open file handle
	dq offset sc27
filebuf QWORD machinecode ; TODO place here LoadFile link: each defined word should link to its loadfile definition
	mov RCX, filereadhandle   ; in HANDLE hfile
	mov RDX, offset infilebf ; out lpBuffer
	xor r8, r8
	mov r8b, infilesize       ; in qWORD nNumberOfBytesToRead
	mov r9, offset nbwr		  ; lpNumberOfBytesRead
	push 0					  ; lpOverlapped at null
	sub rsp, 20h
		call ReadFile
	mov rbx, nbwr
	mov infilebfnumchars, bl ; calculate and save byte, number of characters read
	cmp bl, 0
	jz endoffiledetected      ; if no bytes read, check for error
	mov byte ptr filereadcontinue, 0 ; reports _filebuf_ not required because available characters are present on _infilebf_
	mov rax, offset infilebf
	add rax, rbx
	mov infilebfpastendpos, rax      ; calculate and save position final character of _filebuf_
	jmp donotclosefilehandle
endoffiledetected:
	mov infilebfnumchars, 0 ; indicate no characters are saved
	mov bl, filereadopen
	cmp bl, 0
	jz donotclosefilehandle
	mov byte ptr filereadopen, 0 ; cancel input from designated file handle
	mov al, '>';
	mov noninteractivenotice, al ; change prompt to interactive form i.e. "EI_<" to "EI_>"
	mov rcx, filereadhandle
		call CloseHandle
donotclosefilehandle:
	add rsp, 28h
	pop rbx
	jmp rbx
      endfilebuf:

byte 5, "bufln", 0, msca1 ;    b u f l n    sc27 -> sc28
db offset endbufln - this byte, 0
	dq 73737BD7B002A576h
	sc27 BYTE 5, "bufln", 0 ; fill input line from buffer, internally calling filebuf if required
	dq offset sc28 ; copy from filebuffer to input line up to end of buffer or end of input line
bufln QWORD machinecode ; TODO possible: place here LoadFile link: each defined word should link to its filename and line definition
	xor rbx, rbx
	mov bl, infilebe ; _infilebe_ is offset from infilebf for source index
	mov rsi, offset infilebf
	add rsi, rbx ; calculate source point in line buffer
	;
 	mov byte ptr inlnbe, 0 ; reset inline buffer, as new line is copied in
	mov byte ptr tokenbegin, 0
	mov byte ptr tokenlength, 0 ; reset token posiions also, same reason
	mov rdi, offset inlnbf
	;
	xor rax, rax
	mov al, infilebe ; position in filebuffer

copyone:
	xor rbx, rbx
	mov rbx, infilebfpastendpos
	cmp rsi, rbx ; position one past final character in infilebf
	jge linecopyfinish
	xor rcx, rcx
	mov cl, 1
	mov dl, 0Dh
	cmp dl, byte ptr [rsi]
	cmovz rbx, rcx
	mov dl, 0Ah
	cmp dl, byte ptr [rsi]
	cmovz rbx, rcx
	cmp bl, 1 ; one of special characters cr or lf?
	jz movspace
		movsb
		add al, 1
		jmp copyone
	movspace:
		xor rbx, rbx		
		cmp byte ptr [rsi], 0Ah
		cmovz rbx, rcx
		mov byte ptr [rdi], spc
		add rsi, 1
		add rdi, 1
		add al, 1
		cmp bl, 1
		jz linecopyfinishafterspace
		jmp copyone
charsalreadycopied byte 0 ; on refreshing filebuf, this is the partial line amount
recoveryafterreadbuf qword recoveryafterreadbufc
recoveryafterreadbufc:
			xor rax, rax
			mov rsi, offset infilebf ; infilebf flushed and refilled, so start copy again at start of infilebf
			mov infilebe, al
			jmp copyone
jmp finishretrieve
	linecopyfinish:
		cmp rax, infilesize
		jnz finishretrieve
			xor rbx, rbx
			mov bl, infilebe
			sub rax, rbx
			mov charsalreadycopied, al

			mov rax, recoveryafterreadbuf ; fetch in next buffer
			push rax
			mov rax, offset filebuf
			mov rbx, rax
			jmp qword ptr [rbx]
		finishretrieve:
		mov byte ptr filereadcontinue, 1 ; indicate empty filebuffer
		mov byte ptr [rdi], spc
		cmp al, 0
		jz linecopyfinishcontinue
		add rsi, 1
		add rdi, 1
		add al, 1
		jmp linecopyfinishcontinue
	linecopyfinishafterspace:
		mov byte ptr filereadcontinue, 0 ; indicate nonempty filebuffer
			linecopyfinishcontinue:
		mov cl, infilebe
		mov infilebe, al
		sub al, cl ; calculate characters in this form
		add al, charsalreadycopied
		mov charsalreadycopied, 0
		mov lnbf_numchars, al
		cmp al, 0
		jnz setupprocesslinefrombuffer
			pop rbx ; no need to emit vacuous line
			jmp rbx
		setupprocesslinefrombuffer:
		mov rax, offset processlinefrombuffer
		mov rbx, rax ; exit via tailcall to threaded code _processlinefromfile_
		jmp qword ptr [rbx]
	processlinefrombuffer QWORD colon
	qword emline ; display line copied from file buffer to be processed
	qword crlf
	qword semi
      endbufln:

byte 5, "embuf", 0, msca1 ;    ++ e m b u f ++    db07 -> 0
db offset endembuf - this byte, 0
	dq 989C2AB12C563CAFh
	db07 BYTE 5, "embuf", 0 ; output contents of file buffer, mostly for diagnostics
	dq 0 ;offset sc1D
embuf QWORD machinecode
	sub rsp, 40h ; move "out of the way" for ConcoleOutHandle subroutine <---
	xor rcx, rcx ; on entry, rax and rbx gives the address of this instruction (interpreter, self)
	mov cl, infilesize
	mov r8, rcx	     ; -- 3 -- in number of characters to be written
	mov cl, 0 ; this may be modifiable for a routine like "print remaining line"
	mov rdx, offset infilebf
	add rdx, rcx     ; -- 2 -- pointer to a buffer that contains the characters to be written to the console screen
	lea r9, numchars ; -- 4 -- number of characters actually written
	mov rcx, ConsoleOutHandle ; -- 1 --
    push 0           ; -- 5 -- reserved: must be null
	call WriteConsoleA
	add rsp, 48h
    pop rbx
    jmp rbx
      endembuf:

byte 6, "emfile", 0, msca1 ;    e m f i l e    sc28 -> sc29
db offset endemfile - this byte, 0
	dq 0D96CE26AF33FFF07h
	sc28 BYTE 6, "emfile", 0 ; emit or type out (cat) contents of file to console, without interpreting
	dq offset sc29
	QWORD colon
	qword openfile
	qword checkopenforinfile ; abort if openfile failed
	qword infilereadloop ;
	qword EIreadLn ; if aborted infilereadloop this avoids executing final line reported
	qword semi
byte 18, "checkopenforinfile", 0
	qword 0
checkopenforinfile QWORD machinecode
	mov bl, filereadopen ; check for openfile; nonzero indicates fileopen
	cmp bl, 0 ; nonzero indicates file open
	jnz continueinfileprocess
	add rsp, 2*8 ; set to abort infile for errorcondition
	continueinfileprocess: ;
	pop rbx
	jmp rbx
byte 14, "infilereadloop", 0
	qword 0
infilereadloop QWORD colon
	qword ifemptybuf_filebuf
	qword ifclosed_abort
	qword bufln
	qword tailcall
	qword infilereadloop
returnfromfilebuftag qword returnfromfilebuf

byte 18, "ifemptybuf_filebuf", 0
		dq 0
		ifemptybuf_filebuf QWORD machinecode
	mov al, filereadcontinue ;
	cmp al, 0
	jz returnfromfilebuf
		mov rax, returnfromfilebuftag ; fetch in next buffer
		push rax
		mov rax, offset filebuf
		mov rbx, rax
		jmp qword ptr [rbx]
	returnfromfilebuf:
	pop rbx
	jmp rbx
byte 14, "ifclosed_abort", 0
		dq 0
		ifclosed_abort QWORD machinecode
	mov al, filereadopen
	cmp al, 0
	jnz noabortrequired
		mov al, lnbf_numchars ; declare final line processing complete on detecting end of file
		mov inlnbe, al        ; \ to prevent comment on final line from from leaking into interpreter
		add rsp, 2*8 ; form abort
	noabortrequired:
	pop rbx
	jmp rbx
      endemfile:

byte 6, "infile", 0, msca1 ;    i n f i l e    sc29 -> sc39
db offset endinfile - this byte, 0
	dq 48510E5965237B7Bh
	sc29 BYTE 6, "infile", 0 ; interpret the text file <following token> line per line noninteractively
	dq offset sc30
	QWORD colon
	qword openfile
	qword autoread
	qword checkopenforinfile
	qword ifemptybuf_filebuf
	qword ifclosed_abort
	qword bufln
	qword semi
      endinfile:

byte 7, "default", 0, msca1 ;    __ d e f a u l t __    ac16 -> ac17
db offset enddefault - this byte, 0
    dq 649650800B16097Ch
	ac16 BYTE 7, "default", 0
	dq offset ac17 ;      ; if empty stack, execute the next thread, otherwise skip next thread item
default QWORD machinecode ; \ next thread item usually inserts a usual "default" item on the parameter stack
	mov rax, rbp          ; \ such as mainvocab in the case of searches for token-word modifications
	mov rbx, parameterstackbottom ; offset pstackspace
	cmp rax, rbx
	jz emptystackdefaultnonskip
	mov rax, rsp
	add rax, 8
	mov rbx, qword ptr [rax]
	add rbx, 8
	mov qword ptr [rax], rbx
	emptystackdefaultnonskip:
	pop rbx ; execute next threaded item normally
	jmp rbx
      enddefault:

byte 5, "alias", 0, msca1 ;    __ a l i a s __    ac17 -> ac18
db offset endalias - this byte, 0
    dq 837EE5E23F981FDBh
	ac17 BYTE 5, "alias", 0
	dq ac18
	QWORD chInterp
alias_:	add rax, 8
    mov rbx, rax
	mov rax, qword ptr [rbx]
	jmp QWORD ptr [rax]
      endalias:

byte 8, "refalias", 0, msca1 ;    __ r e f a l i a s __    ac18 -> ac19
db offset endrefalias - this byte, 0
  dq 9DAA2EFCE4D8A2F9h
  ac18 BYTE 8, "refalias", 0 ;
  dq ac19
  QWORD chInterp
refalias: add rax, 8
	mov rcx, qword ptr [rax]
	mov rbx, rcx
	mov rax, qword ptr [rbx] ; TODO fix
	pop rbx ; this cleanly aborts, as the next two lines fail
	jmp rbx
	jmp QWORD ptr [rax] 
      endrefalias:

byte 7, "refhexo", 0, msca1 ;    __ r e f h e x o __    ac19 -> 0    ; test routine for refalias
db offset endrefhexo - this byte, 0
  dq 0E06FEDC37F2E9DD3h
  ac19 BYTE 7, "refhexo", 0 ;
  dq 0
  QWORD refalias
  QWORD hexoutc
      endrefhexo:

byte 8, "finen", 0, msca1 ;    f i n e n    sc30 -> sc31
db offset endfinen - this byte, 0
	dq 73EBEE33EEF927DDh
	sc30 BYTE 5, "finen", 0 ; give final link on vocabulary, useful for linking in a new token-word
	dq offset sc31
finen qword machinecode
	mov rax, rbp
	mov rbx, parameterstackbottom ; offset pstackspace
	sub rax, rbx
	jz finenemptystack

	sub rbp, 8
	mov rbx, [rbp]

	finenafterload:
	sub rbx, 8			; retreat to link position
	mov rax, [rbx]
	cmp rax, 0			; allow for null vocabulary, initial test
	jz finenadv
	mov rbx, rax
	;
	finenext:
	xor rax, rax
	mov al, byte ptr [rbx]
	add rbx, rax
	add rbx, 2 ; advance past char count, and past final senteniel byte 0
	;
	mov rax, qword ptr [rbx] ;
	cmp rax, 0
	jz finenadv
	mov rbx, rax
	jmp finenext
	;
	finenemptystack:
	mov rbx, OFFSET v_sc00 ; sc_vdirset ; rootv default
	jmp finenafterload ; finenext
	;
	finenadv:
	add	rbx, 8			; advance to interpreter position
	mov [rbp], rbx		; last entry
	add rbp, 8
	pop rbx
	jmp rbx
      endfinen:

byte 6, "asmpos", 0, msca1 ;    a s m p o s    sc31 -> sc32
db offset endasmpos - this byte, 0
	dq 1C1CEB9ED3490839h
	sc31 BYTE 6, "asmpos", 0 ; give assembly position for building new tokenwords
	dq offset sc32
	QWORD sysact
asmpos qword ?
      endasmpos:

byte 4, "crea", 0, msca1 ;    c r e a    sc32 -> sc33
db offset endcrea - this byte, offset sc32 - this byte
byte 10, "creaprompt", 0
dq 0
	creaprompt qword textout ; add a new token-word and link it to the end of the vocabulary theraded-list.
	byte 31, "enter token or word to create: ", 0 ; / its interpreter starts out as _noexe_.
	dq 0FE09732AED1091BDh
	sc32 BYTE 4, "crea", 0 ; additionally TODO place in boundary links pxxx and db
	dq offset sc33
crea QWORD colon
	qword token ;
	qword creaprompt
	qword default ; this gives the option of entering a secondary vocabulary into which to insert new tokenword
	qword rootv
	qword emtoken
	qword emspc ; emit spc
	; qword search ; TODO check if token is unique to the vocab
	; verify0
	qword finen
	qword placetoken
	qword semi
	byte 10, "placetoken", 0
	dq 0
placetoken QWORD machinecode
	sub rbp, 8			; place link
	mov rbx, [rbp]
	sub rbx, 8			; retreat to link position
	mov rax, asmpos
	mov [rbx], rax      ; lay down link at finenpos to new entry
	; here link is placed, now place token
    ;
	mov rbx, rax
	mov dh, tokenlength
	mov byte ptr [rbx], dh
	;
	add rbx, 1
	mov asmpos, rbx ; placed token length, now place token char's
	;
	mov rcx, OFFSET inlnbf
	mov rdi, rcx
	xor rax, rax
	mov al, tokenbegin
	add rdi, rax
	  placenexttokenchar: 
	mov cl, BYTE PTR [rdi]
	mov [rbx], cl
	;
	add rbx, 1
	add rdi, 1
	sub dh, 1
	cmp dh, 0
	;
	jz placetokenend
	jmp placenexttokenchar ; -- verified, places first char of token
	;
	  placetokenend:
	xor rax, rax
	mov [rbx], al	; 00 byte
	add rbx, 1
	mov [rbx], rax	; 00 word
	add rbx, 8
	mov rax, rbx
	add rax, 8
	mov rcx, offset noexe
	mov [rbx], rcx ; null-pointer
	add rbx, 8
	mov asmpos, rbx
	;
	pop rbx
	jmp rbx
      endcrea:

byte 2, "HL", 0, msca1 ;    H L    sc33 -> sc34
db offset endHL - this byte, offset sc33 - this byte
HLprompt QWORD textout ; hex loader: poke in assembly code.
	BYTE 4, "HL:>", 0;
HLpromptnoninteractive QWORD textout ;
	BYTE 4, "HL:<", 0
HLreadLn QWORD colon
	qword noninteractivebranch3 ; if detect noninteractive mode, skip ahead to continue at line comment "A" ;
	qword HLprompt
	qword consoleReadln
	qword semi ; end standard interactive line
	qword ifemptybuf_filebuf    ; line comment "A": from detecting noninteractive, retrieve line from filebuf
	qword ifclosed_abort
	qword HLpromptnoninteractive
	qword bufln
	qword semi
	;hiread byte ? ; first char read leftshifted 4
	dq 4ABCB09AC0312140h
	sc33 BYTE 2, "HL", 0 ; hex loader code
	dq offset sc34
HL QWORD machinecode
HLpromptin: ; display prompt and read line
	mov rbx, offset HL01
	push rbx
	mov rbx, offset HLreadLn
	mov rax, rbx ; rax to match EI interpreter colon required jumping to point _colon_
	jmp QWORD ptr [rbx] ; at entry, register rbx is a pointer to the data to interpret
	  HL01:
	mov rax, offset inlnbf
    xor rbx, rbx ; set bl 0
	xor rcx, rcx ; set ch 0
	xor rdx, rdx ; set dl 0 (partial half-byte calculated)
	mov bh, lnbf_numchars
	cmp bh, 2 ; in interactive mode, single command 'Q' ?
	jz HLtestforQ 
	cmp bh, 3 ; in non-interactive mode, single command 'Q'
	jz HLtestforQ 
	jmp HLnotonecharcmd ; single or two characters, not 'Q'
HLtestforQ: ; rax ~ offset inlnbf    bl ~ inlnbe -- bh ~ lnbf_numchars     cl ~ [rax] -- ch ~ hilo flag 0 = low vs. 1 = high half-byte
	mov cl, byte ptr [rax]
	cmp cl, 'Q'
	jz HLfinish
	jmp HLnotonecharcmd
HLnextcharinput: ; skipped first go-around, entry point from HLpromptin is HLnotonecharcmd
	add rax, 1
	add bl, 1
HLnotonecharcmd:   ; rax ~ offset inlnbf    bl ~ inlnbe -- bh ~ lnbf_numchars     cl ~ [rax] -- ch ~ hilo flag
	cmp bh, bl     ; read past end of line?
	jle HLpromptin ; yes - read in new line
	mov cl, byte ptr [rax] ;    

	cmp cl, '/'    ; comment?
	jnz HLnotcomment
	add rbx, 1     ; lookahead one character
	add bl, 1
	mov cl, byte ptr [rax]
	cmp cl, '/'    ; EOL comment
	jz HLpromptin  ; yes, read in next line
	; cmp cl, '@' ; add feature to report address -- TODO
	jmp HLnotonecharcmd ; no, '/' not followed by another '/', ignore first '/' -- jump past rbx and bl increment, as already completed
 HLnotcomment:
	cmp cl, 'a'
	jl notlcnum
	sub cl, 'a' - 'A';
	notlcnum: ; convert lower case 'a' .. 'f' to 'A' .. 'F'

	cmp cl, "A"
	jge HLcharAB
	cmp cl, "0"
	jge HLchar01
	jmp HLnextcharinput		; not recognized - ignore

	  HLchar01:
	cmp cl, "9"
	jle HLchar09
	jmp HLnextcharinput
		
	 HLchar09:
	sub cl, "0"			; "0" <= ch <= "9"
	jmp HLplacenum

	  HLcharAB:
	cmp cl, "F"
	jle HLcharAF
	jmp HLnextcharinput

	  HLcharAF:
	sub cl, "A" - 10		; "A" <= ch <= "F"

	  HLplacenum: ;   rax ~ offset inlnbf    bl ~ inlnbe -- bh ~ lnbf_numchars     cl ~ [rax] -- ch ~ hilo flag        dl ~ highread
	cmp ch, 1
	jz HLplacepair
	mov ch, 1
	shl cl, 4
	mov dl, cl ; highread partial byte-pair
	jmp HLnextcharinput

	  HLplacepair:
	mov ch, 0
	or cl, dl ; highread partial byte-pair

	mov rdi, asmpos
	mov byte ptr [rdi], cl
	add rdi, 1
	mov asmpos, rdi
	jmp HLnextcharinput

HLfinish:	
	mov inlnbe, 2	; do not reparse the "Q"
	cmp ch, 0       ; completed last byte?
	jz HLquitroutine ; yes - quit

; no - quit anyway, ignoring hast byte for now
; a partial pair rests in dl hiread, logic_or it with asmpos location
; assert last partial pair but do not increment address -- currently not active
	;mov rdi, asmpos
	;mov dl, [rdi]
	;and dl, 0Fh
	;or dl, hiread
	;mov [rdi], al

	  HLquitroutine:
	pop rbx
	jmp rbx
      endHL:

byte 2, "xa", 0, msca1 ;    x a    sc34 -> sc35    // execution address
db offset endxa - this byte, offset sc34 - this byte ;
	; BYTE 8, "xaprompt", 0
	; dq 0 ; no link here
	xaprompt QWORD textout
	BYTE 4, "xa >" ; execution address
	dq 838C734105C436BDh
sc34 BYTE 2, "xa", 0
	dq OFFSET sc35		; \ address of token
	QWORD colon			; placing token here just executes as alias ?
	qword token
	qword xaprompt
	qword default
	qword rootv
	qword search
	;qword zabort TODO?
	qword semi
      endxa:

byte 3, "xan", 0, msca1 ;    x a n    sc35 -> sc36    // execution address to name
db offset endxan - this byte, 0 ;
	dq 0E59FEB43D9D95B3h
sc35 byte 3, "xan", 0 ; execution address to name
	dq OFFSET sc36
xan QWORD machinecode
	sub rbp, 8 ; pop from parameter stack pre decrement
	mov rax, qword ptr [rbp]
	cmp rax, 0
	jz namenull
	sub rax, 8 ; currently on offset
	sub rax, 2 ; should be final character before 0
	mov bl, 0 ;
compare:
 	mov bh, byte ptr [rax]
	cmp bh, bl
	jz namecomplete
	sub rax, 1
	add bl, 1
	jmp compare
namecomplete:
	mov qword ptr [rbp], rax
	add rbp, 8
	pop rbx
	jmp rbx
namenull:
	mov qword ptr [rbp], 0
	add rbp, 8
	pop rbx
	jmp rbx
      endxan:

byte 4, "xacn", 0, msca1 ;    x a c n    sc36 -> sc37    // execution address to calling name
db offset endxacn - this byte, 0 ;   
    dq 5A213CECF8B02F88h
sc36 byte 4, "xacn", 0 ; execution address calling name
	dq OFFSET sc37
	QWORD machinecode
	sub rbp, 8 ; pop from parameter stack pre decrement
	mov rax, qword ptr [rbp]
	cmp rax, 0
	jz namenull
	mov rbx, qword ptr [rax]
	sub rbx, 8
	sub rbx, 8 ; currently on offset
	mov rax, rbx
	sub rax, 2 ; should be final character before 0
	mov bl, 0 ;
	jmp compare
      endxacn:

byte 4, "cnxa", 0, msca1 ;    c n x a    sc37 -> sc38    // calling name to execution address
db offset endcnxa - this byte, 0 ;
    dq 01920E887CC83C73h
sc37 byte 4, "cnxa", 0 ; calling name to execution address
	dq OFFSET sc38
	QWORD machinecode
	sub rbp, 8
	mov rax, qword ptr [rbp]
	cmp rax, 0
	jz namenull
	xor rcx, rcx
	mov cl, byte ptr [rax]
	add rax, rcx
	add rax, 10 ; past tokenlength, past final '0', past downlink
	mov qword ptr [rbp], rax
	add rbp, 8
	pop rbx
	jmp rbx
      endcnxa:

byte 4, "emcn", 0, msca1 ;    e m c n    sc38 -> sc39    // emit calling name
db offset endemcn - this byte, 0 ;
    dq 0AB813912AD0349EDh
sc38 byte 4, "emcn", 0 ; emit calling name
	dq offset sc39
emcn QWORD machinecode
	sub rbp, 8
	mov rax, qword ptr [rbp]
	cmp rax, 0
	jz emnamenull
	cmp al, 0
	jz emnamenull
	jmp raxtextout
	emnamenull:
	pop rbx
	jmp rbx
      endemcn:

byte 4, "xanl", 0, msca1 ;    x a n l    sc39 -> sc40    // execution address next link
db offset endxanl - this byte, 0 ;
    dq 0F1FD9CE5BC65108Eh
sc39 byte 4, "xanl", 0 ; execution address next link
    dq offset sc40
xanl QWORD machinecode
	mov rcx, rbp
	sub rcx, 8
	mov rax, qword ptr [rcx]
	sub rax, 8
	mov rdx, qword ptr [rax]
	mov qword ptr [rcx], rdx
	pop rbx
	jmp rbx
      endxanl:

byte 4, "encl", 0, msca1 ;    e n c l    sc40f -> sc41
db offset endencl - this byte, 0
    dq 3933074FF76E2995h
sc40 byte 4, "encl", 0 ;
	dq offset sc41
encl QWORD machinecode
	sub rbp, 8
	mov rax, qword ptr [rbp]
	mov rdi, asmpos
	mov qword ptr [rdi], rax
	add rdi, 8
	mov asmpos, rdi
	pop rbx
	jmp rbx
      endencl:

byte 1, "R", 0, msca1 ;    R    sc41 -> sc42
db offset endR - this byte, 0
    dq 4D9DE37D1D0F2C8Ah
	sc41 BYTE 1, "R", 0 ; repeat the previous line: "R" must be the first and only character on line to activate
	dq offset sc42
	QWORD colon
	qword recopycode ; "R" only valid as first char of input line, and recalling "R" is blocked
	qword semi		; which would be an infinite loop
recopycode QWORD machinecode
	mov al, lnbfPartCopy + 1
	cmp al, spc
	jnz recopynotinfintieloop
	mov al, lnbfPartCopy
	cmp al, "R"
	jz recopyquitinfiniteloop
	  recopynotinfintieloop:
	mov al, lnbf_numchars ; too-long line cannot be restored 
	cmp al, 2 ; inlncopysize	; this invalidates "R" unless it is the only token on input line
	jz recopylineretrievable
	  recopyquitinfiniteloop:
	add rsp, 16			; exit recopy (8) and exit "R" (8)
	pop rbx
	jmp rbx
	  recopylineretrievable:
	mov al, lnbfPartCopyorigsize
	mov lnbf_numchars, al
	mov rdi, offset lnbfPartCopy ; copy the first 
	mov rsi, offset inlnbf
	mov rcx, [rdi]
	mov [rsi], rcx
	add rdi, 8
	add rsi, 8
	mov rcx, [rdi]
	mov [rsi], rcx
	xor al, al
	mov tokenbegin, al
	mov tokenlength, al		; clear token begin and lengths
	mov inlnbe, al			; this simulates re-entering the current input line
	pop rbx
	jmp rbx
      endR:

byte 11, "exitOrReset", 0, msca1 ;    e x i t O r R e s e t    sc42 -> sc43
db offset endexitOrReset - this byte, offset sc42 - this byte ; TODO make as inline querey
	mbtitle byte "Bottom-level Interpreter Ended", 0
	exitprogmsg byte "(Click _NO_ to restart Interpreter and continue.) -- Exit program?", 0
	extrn MessageBoxA: PROC
    dq 85F20F4D13FDD0CBh
	sc42 BYTE 11, "exitOrReset", 0
	dq offset sc43
exitOrReset QWORD exitOrResetc
exitOrResetc: ;modal message box
	mov rax, rsp
	xor rbx, rbx
	mov bl, al
	sub rsp, rbx ; align stack to even boundary
	sub rsp, 30h ;
	mov rcx, 0           ; hWnd = HWND_DESKTOP ; no window owner
	lea rdx, exitprogmsg ; LPCSTR
	lea r8, mbtitle      ; LPCSTR
	mov r9d, 34h           ; uType = MB_YESNO | MB_ICONWARNING
	call MessageBoxA
	cmp rax, 7 ; 7 ~ clicked "no" button; 6 ~ "yes" button
	jnz rewindandend
	jmp restartc ; restart and reload
		;byte "----->"
		;sc0FA byte 12, "rewindandend", 0
		;QWORD 0 ; unsure if this marker is necessary -- enter from _dropOutTrapExitOrReset_
 		;QWORD machinecode
rewindandend: ; restore registers and return to c++ stub
	mov filereadinterrupt, 1 ; if in noninteractive mode, suspend next automatic read
	mov rbp, holdgivenRBP ; rbp_hold
	xor rbx, rbx ; zero these registers initially zero
	mov rdi, rbx
	mov rsi, rbx
	mov r12, rbx
	mov r13, rbx
	mov r14, rbx
	mov r15, rbx
	mov rsp, holdgivenRSP
	ret ; extrn ExitProcess: PROC
      endexitOrReset:

byte 2, "bv", 0, msca1 ;    b v    sc43 -> sc44
db offset endbv - this byte, 0
    dq 0C7FD61E7BBDA208Ch
sc43 BYTE 2, "bv", 0 ;    execution interpreter vocabulary
	dq sc44 ;
bv QWORD sysvar
	qword offset v_b00
	;byte 0 ; these would be placed to vocabulary lead, i.e. bv_sub
	;byte 0
    ;
    dq 0C7FD61E7BBDA208Ch ; unsure if this tag is here, but it seems to work
  BYTE 2, "bv", 0 ;
dq offset b00
v_b00 QWORD noexe
    dq 0C7FD61E7BBDA208Ch
b00	BYTE 2, "bv", 0 ; same name as sc27, but in vocabulary bv
	qword 0 ; ususally nexe offset i.e. b01
bv_sub QWORD sysvar
	qword offset v_b00
	byte 0
	byte 0
      endbv:

byte 9, "usinglist", 0, msca1 ;    u s i n g l i s t    sc44 -> sc45
db offset endusinglist - this byte, offset sc44 - this byte
	dq 0 ; reference to additional usinglists, eight items per usinglist
	dq 9B11FD0531DD72D0h
sc44 BYTE 9, "usinglist", 0
	qword sc45
usinglist QWORD sysact ; TODO we want a separate using option that lists items in usinglist -- object message?
	dq 0
    dq 7 dup (0)
      endusinglist:

sc29start byte 5, "using", 0, msca1 ;    u s i n g    sc45 -> sc46
db offset endusing - this byte, 0
    dq 0E1D4940F8FFF83B9h
sc45 BYTE 5, "using", 0
	dq sc46
using QWORD machinecode
	mov rax, parameterstackbottom ; offset pstackspace
	cmp rbp, rax
	jz usinglistendfound ; -- no stack entry, skip all
	sub rbp, 8
	mov rax, qword ptr [rbp]
	cmp rax, 0 ; 0 on stack, remove final entry from usinglist
	jz clearfinalentry

	mov rcx, 8
	mov rbx, offset usinglist
	    usinglistadvance:
	add rbx, 8 ; advance to first/next itme there
	sub rcx, 1
	cmp rcx, 0 ; using list full?
	jz usinglistfinished ; yes, finish
	mov rax, qword ptr [rbx]
	cmp rax, 0
	jz usinglistfoundzero
	jmp usinglistadvance

	usinglistfoundzero:
	mov rax, qword ptr [rbp]
	mov qword ptr [rbx], rax
	jmp usinglistfinished

	clearfinalentry:
	mov rbx, offset usinglist
	add rbx, 8 ; advance to first item in usinglist
	mov rax, qword ptr [rbx]
	cmp rax, 0
	jz usinglistfinished; -- initial usinglist empty -- no action needed

	; TODO test for full usinglist
	  usinglistfindemptyadvance: 
	add rbx, 8
	mov rax, qword ptr [rbx]
	cmp rax, 0
	jz usinglistendfound
	jmp usinglistfindemptyadvance

usinglistendfound:
	sub rbx, 8
	mov rax, 0
	mov qword ptr [rbx], rax

	usinglistfinished:
	pop rbx
	jmp rbx 
      endusing:

byte 3, "alu", 0, msca1 ; for traceback ;    a l u    sc46 -> 0
db offset endalu - this byte, sc46 - this byte
    dq offset alu00 ; stub-link for some searching routines
    v_alu00 QWORD noexe
	dq 0ABC3AF77714C3B51h
	alu00 BYTE 3, "alu", 0 ; same name as sc30, but in vocabulary alu
	qword 0
alu_sub QWORD vocab
	qword offset v_alu00
	byte 0 ; sublinks possible in _ac_ subvocabulary
	dq 0ABC3AF77714C3B51h
sc46 BYTE 3, "alu", 0
	dq 0
alu QWORD sysvar
	qword offset v_alu00
	  endalu:

	msca_rutime equ 80h ; sectiontype indicator code: entire memory section allocation
byte 17, "RuntimeBuiltCodes", 0, msca_rutime
	RuntimeBuiltCodesBegin:
	RuntimeBuildCodes DB 16384 DUP (?) ; hold 16 Kib for assembly
lomthrin_fn ENDP
lomthrin ENDS
END